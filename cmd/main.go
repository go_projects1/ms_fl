package main

import (
	"context"
	"dbo/onboarding/bootstrap"
	"dbo/onboarding/config"
	"dbo/onboarding/pkg/logger"
	"fmt"
	"os"
	"os/signal"

	_ "github.com/lib/pq"
)

// @title Onboarding Physical
// @contact.name API Support
// @contact.url https://hamkorbank.uz/
// @contact.email mdtsoy@yandex.ru
func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	cfg := config.Load()
	l := logger.New(cfg.LogLevel, "onboarding_ph")
	application := bootstrap.New(cfg)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		l.Info(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()
	application.Run(ctx)
}
