package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"errors"
)

type FindProspectIn struct {
	ProspectID string
	ProductID  string
}

type FindProspectOut struct {
	Prospect entities.Prospect
	Offer    entities.Offer
}

type findProspect struct {
	localSource   contracts.IProspectSource
	offerSource   contracts.IMasterOfferSource
	consentSource contracts.IProspectConsentWithVersion
	appealsStore  contracts.IAppealStore
	in            *FindProspectIn
	out           *FindProspectOut
}

func FindProspect(prospectSource contracts.IProspectSource,
	offerSource contracts.IMasterOfferSource,
	consentSource contracts.IProspectConsentWithVersion,
	appealsStore contracts.IAppealStore,
) *findProspect {
	return &findProspect{
		localSource:   prospectSource,
		offerSource:   offerSource,
		consentSource: consentSource,
		appealsStore:  appealsStore,
	}
}

func (uc *findProspect) Result() *FindProspectOut {
	return uc.out
}

func (uc *findProspect) Params(in FindProspectIn) {
	uc.in = &in
}

func (uc *findProspect) Execute(ctx context.Context) error {
	prospect, err := uc.localSource.Get(ctx, uc.in.ProspectID)
	if err != nil {
		return err
	}
	if uc.in.ProductID != "" {
		appeal := entities.Appeal{
			ProspectID: prospect.ID,
			ProductID:  uc.in.ProductID,
		}
		_, err = uc.appealsStore.Store(ctx, appeal)
		if err != nil {
			return err
		}
	}
	offer, err := uc.offerSource.GetOffer(ctx, entities.ConsentCode, "ru")
	if err != nil {
		return err
	}
	consent, err := uc.consentSource.GetByProspectAndVersion(ctx, prospect.ID, offer.Code, offer.LatestVersionCode)
	if err != nil {
		if !errors.Is(err, errs.ErrNotFound) {
			return err
		}
		prospect.IsConsentNeeded = true
	}
	prospect.IsConsentNeeded = prospect.IsConsentNeeded || offer.LatestVersionCode != consent.OfferVersionCode
	uc.out = &FindProspectOut{
		Prospect: prospect,
		Offer:    offer,
	}
	return nil
}
