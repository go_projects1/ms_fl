package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
)

type InFillMissing struct {
	Profile    entities.Profile
	ProspectID string
}

type OutFillMissing struct {
	Prospect entities.Prospect
}

type FillMissingAndApprove struct {
	in                    InFillMissing
	out                   *OutFillMissing
	LocalProspectSource   contracts.IProspectSource
	LocalProfileRetriever contracts.IProfileRetriever
	ProfileIABS           contracts.IProfileSearcher
	References            contracts.IReferences
	AddressLoader         contracts.IAddressLoader
	ProfileSaver          contracts.IProfileSaver
	ProspectSaver         contracts.IProspectSaver
}

func (uc *FillMissingAndApprove) Result() *OutFillMissing {
	return uc.out
}

func (uc *FillMissingAndApprove) Params(params InFillMissing) {
	uc.in = params
}

func (uc *FillMissingAndApprove) Execute(ctx context.Context) error {
	prospect, err := uc.LocalProspectSource.Get(ctx, uc.in.ProspectID)
	if err != nil {
		return err
	}
	var profileIABS entities.Profile
	if prospect.IsClient() {
		profileIABS, err = uc.ProfileIABS.Search(ctx, entities.IdentityData{IABSClientID: prospect.IABSClientID})
		if err != nil {
			return err
		}
	}
	if uc.in.Profile.Source != entities.ProfileSourceUserInput {
		return errs.Errf(errs.ErrWrongInput, "Wrong source %d", uc.in.Profile.Source)
	}

	profiles, err := uc.LocalProfileRetriever.GetByProspect(ctx, prospect.ID)
	if err != nil {
		return err
	}
	for i := range profiles {
		addresses, err := uc.AddressLoader.GetByProfile(ctx, profiles[i].ID)
		if err != nil {
			return err
		}
		profiles[i].SetAddresses(addresses)
	}
	if prospect.IsClient() {
		profiles = append(profiles, profileIABS)
	}
	profiles = append(profiles, uc.in.Profile)
	profile, err := entities.MergeProfiles(profiles)
	if err != nil {
		return err
	}
	codes, err := uc.References.Get(
		ctx,
		entities.RefDirectoryRegion,
		entities.RefDirectoryDistrict,
		entities.RefDirectoryCountryCode,
		entities.RefDirectoryCitizenshipCode)
	if err != nil {
		return err
	}
	profile, spec, err := profile.Validate(codes)
	if err != nil {
		return err
	}
	if len(spec) > 0 {
		return errs.ErrValidation
	}
	prevFullProfile := entities.Profiles(profiles).GetFirstBySource(entities.ProfileSourceLocal)
	saveProfileFunc := uc.ProfileSaver.Store
	if prevFullProfile != nil {
		profile.ID = prevFullProfile.ID
		saveProfileFunc = uc.ProfileSaver.Update
	}
	uc.in.Profile.ProspectID = prospect.ID
	uc.in.Profile, err = saveProfileFunc(ctx, uc.in.Profile)
	if err != nil {
		return err
	}
	prospect.Profiles = profiles
	prospect.FullProfile = &profile
	prospect.IsProfileComplete = true

	prospect, err = uc.ProspectSaver.Update(ctx, prospect)
	if err != nil {
		return err
	}
	uc.out = &OutFillMissing{
		Prospect: prospect,
	}
	return nil
}
