package usecases

import "time"

func now() *time.Time {
	t := time.Now()
	return &t
}
