package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"errors"
	"sync"
)

/**
UseCase: Identify - Процесс поиска по ДУЛ
1. Ищем в ИАБС
2. Ищем в НИББД
3. Если не найден в НИББД - выкидываем ошибку.
4. Сохраняем профиль из НИББД
5. Собираем карточку клиента.
6. Валидируем/Собираем поля необходимые для заполнения пользователем

Предыдущий логический процесс - ProspectByPhone/ProspectByIABS: получение проспекта
Следующий логический процесс - FillMissingAndApprove: подтверждение/дозаполнение карточки пользователем -

*/

type InIdentify struct {
	ProspectID string
	Identity   entities.IdentityData
}

type OutIdentify struct {
	Prospect entities.Prospect
	Specs    entities.ProfileSpec
}

type Identify struct {
	in  InIdentify
	out *OutIdentify

	ProspectSourceRepo contracts.IProspectSource
	ProfilesIABS       contracts.IProfileSearcher
	ProfilesNIBBD      contracts.IProfileSearcher
	ProfileSaverRepo   contracts.IProfileSaver
	References         contracts.IReferences
	ProspectSaverRepo  contracts.IProspectSaver
	AddressSaverRepo   contracts.IAddressSaver
}

func (uc *Identify) Params(identify InIdentify) {
	uc.in = identify
}

func (uc *Identify) Result() *OutIdentify {
	return uc.out
}

func (uc *Identify) Execute(ctx context.Context) error {
	prospect, err := uc.ProspectSourceRepo.Get(ctx, uc.in.ProspectID)
	if err != nil {
		return err
	}
	var wg sync.WaitGroup
	var profileIABS, profileNIBBD entities.Profile
	var errIABS, errNIBBD error
	wg.Add(1)
	go func() {
		defer wg.Done()
		profileIABS, errIABS = uc.ProfilesIABS.Search(ctx, uc.in.Identity)
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		profileNIBBD, errNIBBD = uc.ProfilesNIBBD.Search(ctx, uc.in.Identity)
	}()
	wg.Wait()

	if errNIBBD != nil || (errIABS != nil && !errors.Is(errIABS, errs.ErrNotFound)) {
		if errNIBBD != nil {
			err = errNIBBD
		}
		if errIABS != nil {
			err = errIABS
		}
		return err
	}
	if errIABS == nil {
		prospect.Profiles = append(prospect.Profiles, profileIABS)
	}
	prospect.Profiles = append(prospect.Profiles, profileNIBBD)
	profileNIBBD.ProspectID = prospect.ID
	savedProfile, err := uc.ProfileSaverRepo.Store(ctx, profileNIBBD)
	if err != nil {
		return err
	}
	addr := profileNIBBD.Addresses()
	for i := range addr {
		addr[i].ProfileID = savedProfile.ID
		addr[i], err = uc.AddressSaverRepo.Store(ctx, addr[i])
		if err != nil {
			return err
		}
	}
	var fullProfile entities.Profile
	fullProfile, err = entities.MergeProfiles(prospect.Profiles)
	if err != nil {
		return err
	}

	codes, err := uc.References.Get(ctx,
		entities.RefDirectoryRegion,
		entities.RefDirectoryDistrict,
		entities.RefDirectoryCountryCode,
		entities.RefDirectoryNationalityCode,
		entities.RefDirectoryCitizenshipCode)
	if err != nil {
		return err
	}
	var spec entities.ProfileSpec
	fullProfile, spec, err = fullProfile.Validate(codes)
	if err != nil {
		return err
	}
	prospect, err = uc._updateProspectIdentity(ctx, prospect, fullProfile)
	if err != nil {
		return err
	}
	uc.out = &OutIdentify{
		Prospect: prospect,
		Specs:    spec,
	}
	return nil
}

func (uc *Identify) _updateProspectIdentity(
	ctx context.Context, prospect entities.Prospect, fullProfile entities.Profile) (entities.Prospect, error) {
	prospect.FullProfile = &fullProfile
	prospect.IdentityData = entities.IdentityData{
		PINFL:          fullProfile.PINFL,
		DocSeries:      fullProfile.DocSeries,
		DocNumber:      fullProfile.DocNumber,
		BirthDate:      fullProfile.BirthDate,
		IABSClientID:   fullProfile.IABSClientID,
		IABSClientCode: fullProfile.IABSClientCode,
	}
	prospect.State = entities.ProspectStateIdentityFilled

	return uc.ProspectSaverRepo.Update(ctx, prospect)
}
