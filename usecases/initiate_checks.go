package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
)

type InCheckProspect struct {
	Prospect entities.Prospect
}
type OutCheckProspect InCheckProspect

// CheckProspect - Запуск проверок и сохранение результата
type CheckProspect struct {
	in            InCheckProspect
	checker       contracts.IChecker
	prospectSaver contracts.IProspectSaver
	out           *OutCheckProspect
}

func (uc *CheckProspect) Params(prospect InCheckProspect) {
	uc.in = prospect
}

func (uc *CheckProspect) Result() *OutCheckProspect {
	return uc.out
}

func (uc *CheckProspect) Execute(ctx context.Context) error {
	p := uc.in.Prospect
	results, err := uc.checker.Check(ctx, uc.in.Prospect)
	if err != nil {
		return err
	}
	p.IsAffiliated = results.IsAffiliated()
	p.IsTerrorist = results.IsTerrorist()
	p.CheckedAt = now()
	if results.IsDocActualizationRequired() {
		p.State = entities.ProspectStateContactVerified
	}
	p, err = uc.prospectSaver.Update(ctx, p)
	if err != nil {
		return err
	}
	uc.out = &OutCheckProspect{Prospect: p}
	return nil
}
