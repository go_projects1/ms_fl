package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
)

type GenerateOTPIn struct {
	ProspectID string
}

type GenerateOTPOut struct {
	SignatureID string
}

type generateOTP struct {
	signatureGenerator contracts.ISignatureService
	contactSource      contracts.IContactGetter
	in                 *GenerateOTPIn
	out                *GenerateOTPOut
}

func GenerateOTP(signatureService contracts.ISignatureService, contactGetter contracts.IContactGetter) *generateOTP {
	return &generateOTP{
		signatureGenerator: signatureService,
		contactSource:      contactGetter,
		in:                 nil,
		out:                nil,
	}
}

func (uc *generateOTP) Params(in GenerateOTPIn) {
	uc.in = &in
}

func (uc *generateOTP) Results() *GenerateOTPOut {
	return uc.out
}

func (uc *generateOTP) Execute(ctx context.Context) error {
	contacts, err := uc.contactSource.GetByProspect(ctx, uc.in.ProspectID)
	if err != nil {
		return err
	}
	var contact entities.Contact
	for _, c := range contacts {
		if c.IsVerified() &&
			(c.Type == entities.ContactTypePhoneMobile ||
				c.Type == entities.ContactTypePhoneMobileMB) {
			contact = c
			break
		}
	}
	if contact.ID == "" {
		return errs.Errf(errs.ErrNotFound, "No matching Contact found")
	}

	signID, err := uc.signatureGenerator.InitSign(ctx, contact)
	if err != nil {
		return err
	}
	uc.out = &GenerateOTPOut{SignatureID: signID}
	return nil
}

type VerifyOTPIn struct {
	Offer       entities.Offer
	Code        string
	SignatureID string
	ProspectID  string
}

type VerifyOTPOut struct {
	IsSuccess bool
}

type verifyOTP struct {
	signatureGenerator contracts.ISignatureService
	consentStore       contracts.IConsentSaver
	in                 *VerifyOTPIn
	out                *VerifyOTPOut
}

func VerifyOTP(signatureService contracts.ISignatureService, consentSaver contracts.IConsentSaver) *verifyOTP {
	return &verifyOTP{
		signatureGenerator: signatureService,
		consentStore:       consentSaver,
		in:                 nil,
		out:                nil,
	}
}

func (uc *verifyOTP) Params(in VerifyOTPIn) {
	uc.in = &in
}

func (uc *verifyOTP) Results() *VerifyOTPOut {
	return uc.out
}

func (uc *verifyOTP) Execute(ctx context.Context) error {
	err := uc.signatureGenerator.Sign(ctx, uc.in.Code, uc.in.SignatureID)
	if err != nil {
		return err
	}
	consent := entities.Consent{
		ProspectID:       uc.in.ProspectID,
		SignID:           uc.in.SignatureID,
		OfferCode:        uc.in.Offer.Code,
		OfferVersionCode: uc.in.Offer.LatestVersionCode,
		AcceptedAt:       now(),
	}
	_, err = uc.consentStore.Store(ctx, consent)
	if err != nil {
		return err
	}
	uc.out = &VerifyOTPOut{IsSuccess: true}
	return nil
}
