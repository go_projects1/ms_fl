package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"errors"
	"time"
)

// InProspectByPhone - Входящие параметры
type InProspectByPhone struct {
	Contact   entities.Contact
	ProductID string
	UserID    string
}

// OutProspectByPhone - Выходящие параметры
type OutProspectByPhone struct {
	Prospect entities.Prospect
}

// ProspectByPhone - UseCase используется для поиска и создания проспекта по номеру телефона.
type ProspectByPhone struct {
	in                    *InProspectByPhone
	ProspectByPhoneFinder contracts.IProspectByContactFinder
	ProspectSaver         contracts.IProspectSaver
	ContactSaver          contracts.IContactSaver
	out                   *OutProspectByPhone
}

// Params - Присвоить Входящие параметры
func (uc *ProspectByPhone) Params(in InProspectByPhone) {
	uc.in = &in
	uc.in.Contact.VerifiedAt = now()
}

// Result - Получить выходящие параметры
func (uc *ProspectByPhone) Result() *OutProspectByPhone {
	return uc.out
}

// Execute - Реализация логики бизнес процесса
func (uc *ProspectByPhone) Execute(ctx context.Context) error {
	var prospect entities.Prospect
	var err error
	prospect, err = uc.ProspectByPhoneFinder.GetByContact(ctx, uc.in.Contact)
	if err == nil {
		uc.out = &OutProspectByPhone{Prospect: prospect}
		return nil
	}
	if !errors.Is(err, errs.ErrNotFound) {
		return err
	}
	// TODO check if verified contact found of other type
	prospect, err = uc._createProspect(ctx)
	if err != nil {
		return err
	}
	uc.out = &OutProspectByPhone{Prospect: prospect}
	return nil
}

func (uc *ProspectByPhone) _createProspect(ctx context.Context) (entities.Prospect, error) {
	var prospect entities.Prospect
	var err error
	prospect = entities.Prospect{
		State:             entities.ProspectStateContactVerified,
		RSClientID:        "",
		IsAffiliated:      false,
		IsTerrorist:       false,
		IsProfileComplete: false,
	}
	prospect, err = uc.ProspectSaver.Store(ctx, prospect)
	if err != nil {
		return prospect, err
	}
	c := uc.in.Contact
	now := time.Now()
	c.ProspectID = prospect.ID
	c.VerifiedAt = &now
	c, err = uc.ContactSaver.Store(ctx, c)
	if err != nil {
		return prospect, err
	}
	uc.in.Contact = c
	return prospect, nil
}
