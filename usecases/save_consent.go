package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
)

type InSaveConsent struct {
	Consent entities.Consent
}

type OutSaveConsent InSaveConsent

// SaveConsent - UseCase Сохранения СОПД в статусе "ПОДПИСАН"
type SaveConsent struct {
	in             *InSaveConsent
	ConsentSaver   contracts.IConsentSaver
	ProspectSource contracts.IProspectSource
	ProspectSaver  contracts.IProspectSaver
	out            *OutSaveConsent
}

func (uc *SaveConsent) Execute(ctx context.Context) error {
	consent := uc.in.Consent
	if err := consent.Validate(); err != nil {
		return errs.ErrValidation
	}
	if !consent.IsAccepted() {
		return errs.ErrValidation
	}
	p, err := uc.ProspectSource.Get(ctx, consent.ProspectID)
	if err != nil {
		return err
	}

	consent, err = uc.ConsentSaver.Store(ctx, consent)
	if err != nil {
		return err // Should be "errs.ErrSourceInternal"
	}
	p.IsConsentNeeded = false
	if _, err := uc.ProspectSaver.Update(ctx, p); err != nil {
		return err
	}
	uc.out = &OutSaveConsent{consent}
	return nil
}

func (uc *SaveConsent) Params(in InSaveConsent) {
	uc.in = &in
}

func (uc *SaveConsent) Result() *OutSaveConsent {
	return uc.out
}
