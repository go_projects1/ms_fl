package usecases

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
	errs "dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"errors"
)

type InProspectByIABS struct {
	Contact      entities.Contact
	IABSClientID string
}

type OutProspectByIABS struct {
	Prospect entities.Prospect
}

// ProspectByIABS - используется для поиска и создания проспекта по номеру телефона.
// HamkorStore дергает Онбоардинг с номером телефона и ID ИАБС используется этот Кейс
//
// uc := ProspectByIABS{}
// if err := uc.Execute(); err != nil {
// // Handle Error....
// }
// if r := uc.Result(); r != nil {
// fmt.Printf("%+v", r)
// }
type ProspectByIABS struct {
	in  *InProspectByIABS
	out *OutProspectByIABS
	logger.Logger
	LocalProspectByContactFinder contracts.IProspectByContactFinder
	LocalProspectFinder          contracts.IProspectFinder
	ProspectSaver                contracts.IProspectSaver
	IabsProfileFinder            contracts.IProfileSearcher
	ContactSaver                 contracts.IContactSaver
	LocalContactSource           contracts.IContactGetter
}

func (uc *ProspectByIABS) Params(in InProspectByIABS) {
	uc.in = &in
	uc.in.Contact.VerifiedAt = now()
}

// Result returns result if Execute did not return error
func (uc *ProspectByIABS) Result() *OutProspectByIABS {
	return uc.out
}

// Execute Use case and returns error in case it occurred. call uc.Result() if err did not happen
func (uc *ProspectByIABS) Execute(ctx context.Context) error {
	if uc.in == nil {
		return errs.ErrUCNoInputIsProvided
	}
	if uc.in.Contact.Value == "" || uc.in.Contact.Type != entities.ContactTypePhoneMobile {
		return errs.ErrValidation
	}
	var prospect entities.Prospect
	var err error
	identity := entities.IdentityData{
		IABSClientID: uc.in.IABSClientID,
	}
	// Ищем локально проспекта с данным ИАБС ИД
	prospect, err = uc.LocalProspectFinder.Find(ctx, identity)
	if err == nil {
		return uc._matchOrBindPhone(ctx, prospect)
	}
	uc.Error(err.Error())
	if !errors.Is(err, errs.ErrNotFound) {
		return err
	}
	// Ищем по контакту. Если находим, создаем контакт
	prospect, err = uc._findOrCreateByContact(ctx, identity)
	if err != nil {
		return err
	}
	uc.out = &OutProspectByIABS{Prospect: prospect}
	return nil
}

func (uc *ProspectByIABS) _matchOrBindPhone(ctx context.Context, prospect entities.Prospect) error {
	contacts, err := uc.LocalContactSource.GetByProspect(ctx, prospect.ID)
	if err != nil {
		return err
	}
	// Ищем контакт, который пришел вместе с ID ИАБСа
	for _, c := range contacts {
		if c.Type != uc.in.Contact.Type || c.Value != uc.in.Contact.Value {
			continue
		}
		shouldSaveContact := false
		if !c.IsVerified() {
			uc.in.Contact.VerifiedAt = now()
			shouldSaveContact = true
		} else {
			uc.in.Contact.VerifiedAt = c.VerifiedAt
		}
		uc.in.Contact.ID = c.ID
		uc.in.Contact.ProspectID = prospect.ID
		if shouldSaveContact {
			uc.in.Contact, err = uc.ContactSaver.Update(ctx, uc.in.Contact)
		}
		if err != nil {
			return err
		}
		break
	}
	if uc.in.Contact.ID == "" {
		uc.in.Contact.ProspectID = prospect.ID
		uc.in.Contact.VerifiedAt = now()
		uc.in.Contact, err = uc.ContactSaver.Store(ctx, uc.in.Contact)
		if err != nil {
			return err
		}
	}
	uc.out = &OutProspectByIABS{Prospect: prospect}
	return nil
}

func (uc *ProspectByIABS) _findOrCreateByContact(ctx context.Context, identity entities.IdentityData) (entities.Prospect, error) {
	prospect, err := uc.LocalProspectByContactFinder.GetByContact(ctx, uc.in.Contact)
	if err == nil {
		return prospect, nil
	}
	uc.Error(err.Error())
	iabsProfile, err := uc.IabsProfileFinder.Search(ctx, identity)
	if err != nil {
		uc.Error(err.Error())
		return prospect, err
	}
	identity.PINFL = iabsProfile.PINFL
	identity.BirthDate = iabsProfile.BirthDate
	identity.DocSeries = iabsProfile.DocSeries
	identity.DocNumber = iabsProfile.DocNumber
	prospect = entities.Prospect{
		IdentityData:      identity,
		State:             entities.ProspectStateIdentified,
		IsAffiliated:      false,
		IsTerrorist:       false,
		IsProfileComplete: false,
		VerifiedAt:        nil,
		CheckedAt:         nil,
		Contacts:          nil,
	}
	prospect, err = uc.ProspectSaver.Store(ctx, prospect)
	if err != nil {
		return prospect, err
	}
	uc.in.Contact.ProspectID = prospect.ID
	uc.in.Contact.VerifiedAt = now()
	uc.in.Contact, err = uc.ContactSaver.Store(ctx, uc.in.Contact)
	if err != nil {
		return prospect, err
	}
	return prospect, nil
}
