package bootstrap

import (
	"context"
	"dbo/onboarding/config"
	"dbo/onboarding/controller"
	"dbo/onboarding/drivers/services"
	"dbo/onboarding/drivers/services/service_registry"
	"dbo/onboarding/drivers/store"
	"dbo/onboarding/drivers/store/dbstore"
	"dbo/onboarding/gateways/grpc"
	"dbo/onboarding/gateways/mq"
	"dbo/onboarding/gateways/rest"
	"dbo/onboarding/pkg/logger"
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.hamkorbank.uz/libs/rabbit"
)

const rmqReconnectDelayInSeconds = 10
const gracefulDeadline = 5 * time.Second

type App struct {
	db         *sqlx.DB
	store      store.Store
	controller *controller.Controller
	container  services.Container
	mqPool     *mq.Pool
	rest       *http.Server
	grpc       *grpc.GrpcServer

	teardown []func()
}

func New(cfg config.Config) *App {
	teardown := make([]func(), 0)
	log := logger.New(cfg.LogLevel, "dbo_physical_onboarding")
	amqp, err := rabbit.Dial(cfg.AMQPEndpoint, log, rmqReconnectDelayInSeconds)
	if err != nil {
		panic(fmt.Sprintf("connection.open: %s", err.Error()))
	}
	log.Info("AMQP connection established")
	teardown = append(teardown, func() {
		log.Info("AMQP connection closing...")
		if err := amqp.Close(); err != nil {
			log.Error(err.Error())
		}
		log.Info("AMQP connection closed")
	})

	// Подключение к БД
	db, err := sqlx.Connect("postgres", cfg.PostgresURL())
	if err != nil {
		panic(err)
	}
	log.Info("Database connection established")
	teardown = append(teardown, func() {
		log.Info("Database connection closing...")
		if err := db.Close(); err != nil {
			log.Error(err.Error())
		}
		log.Info("Database connection closed")
	})
	storage := dbstore.New(log, db)
	serviceContainer := service_registry.New(cfg, log)
	ctrl := controller.New(log, &serviceContainer, storage)

	httpSrv := rest.New(cfg.HTTPPort, log, ctrl, serviceContainer.Security(), serviceContainer.ProductURL())
	teardown = append(teardown, func() {
		log.Info("HTTP is shutting down")
		ctxShutDown, cancel := context.WithTimeout(context.Background(), gracefulDeadline)
		defer cancel()
		if err = httpSrv.Shutdown(ctxShutDown); err != nil {
			log.Error(fmt.Sprintf("server Shutdown Failed:%s", err))
			if err == http.ErrServerClosed {
				err = nil
			}
			return
		}
		log.Info("HTTP is shut down")
	})

	app := App{
		db:         db,
		store:      storage,
		controller: &ctrl,
		container:  &serviceContainer,
		teardown:   teardown,
		mqPool:     mq.NewPool(&ctrl, log, amqp),
		rest:       httpSrv,
		grpc:       grpc.New(log, &ctrl, cfg.GRPCPort),
	}
	return &app
}

func (app *App) Run(ctx context.Context) {
	go func(c context.Context) {
		app.mqPool.Run(c)
	}(ctx)
	go func() {
		if err := app.rest.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	go func(c context.Context) {
		if err := app.grpc.Run(c); err != nil {
			panic(err)
		}
	}(ctx)
	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}
}
