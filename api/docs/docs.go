// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag

package docs

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "API Support",
            "url": "https://hamkorbank.uz/",
            "email": "mdtsoy@yandex.ru"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/public/approve": {
            "post": {
                "description": "saves user input as profile by user input. Will use it as least prioritized. Returns redirect URL and ctoken",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "BFF"
                ],
                "summary": "Receives user input and prospect confirmation",
                "parameters": [
                    {
                        "description": "Missing fields in profile",
                        "name": "reqFillMissing",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/rest.reqFillMissing"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/rest.R"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/rest.resFillMissing"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "422": {
                        "description": "Unprocessable Entity",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    }
                }
            }
        },
        "/public/identify": {
            "post": {
                "description": "Provides information about prospect. Based on statuses decision should be made",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "BFF"
                ],
                "summary": "initial request on onboarding start",
                "parameters": [
                    {
                        "description": "DocSeries|DocNumber|BirthDate + ctoken",
                        "name": "reqIdentify",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/rest.reqIdentify"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/rest.R"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/rest.respInit"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "422": {
                        "description": "Unprocessable Entity",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    }
                }
            }
        },
        "/public/initialize": {
            "get": {
                "description": "Provides information about prospect. Based on statuses decision should be made",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "BFF"
                ],
                "summary": "initial request on onboarding start",
                "parameters": [
                    {
                        "type": "string",
                        "description": "ctoken",
                        "name": "token",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/rest.R"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/rest.respInit"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "422": {
                        "description": "Unprocessable Entity",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    }
                }
            }
        },
        "/public/sign-init": {
            "post": {
                "description": "Sends Authorization code to verified Contact of type MobilePhone | MobileBanking",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "BFF"
                ],
                "summary": "Request to sign offer with OTP",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Token containing ProspectID",
                        "name": "ctoken",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/rest.R"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/rest.initSignRes"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "422": {
                        "description": "Unprocessable Entity",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    }
                }
            }
        },
        "/public/sign-verify": {
            "post": {
                "description": "Checks OTP and saves OTP fact in case of success based on prospectID generated from token",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "BFF"
                ],
                "summary": "Checks code input",
                "parameters": [
                    {
                        "description": "Sign ID version code etc",
                        "name": "request",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/rest.initSignReq"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "422": {
                        "description": "Unprocessable Entity",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/rest.R"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "rest.R": {
            "type": "object",
            "properties": {
                "data": {
                    "type": "object"
                },
                "error_code": {
                    "type": "integer"
                },
                "error_note": {
                    "type": "string"
                },
                "status": {
                    "type": "string"
                }
            }
        },
        "rest.initSignReq": {
            "type": "object",
            "required": [
                "code",
                "ctoken",
                "offer_code",
                "offer_version_code",
                "sign_id"
            ],
            "properties": {
                "code": {
                    "type": "string"
                },
                "ctoken": {
                    "type": "string"
                },
                "offer_code": {
                    "type": "string"
                },
                "offer_version_code": {
                    "type": "string"
                },
                "sign_id": {
                    "type": "string"
                }
            }
        },
        "rest.initSignRes": {
            "type": "object",
            "properties": {
                "sign_id": {
                    "type": "string"
                }
            }
        },
        "rest.offer": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "string"
                },
                "content": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "sign_required": {
                    "type": "boolean"
                },
                "version_code": {
                    "type": "string"
                }
            }
        },
        "rest.prospect": {
            "type": "object",
            "properties": {
                "birth_of_date": {
                    "type": "string"
                },
                "doc_number": {
                    "type": "string"
                },
                "doc_series": {
                    "type": "string"
                },
                "pinfl": {
                    "type": "string"
                },
                "state": {
                    "type": "integer"
                }
            }
        },
        "rest.reqFillMissing": {
            "type": "object",
            "properties": {
                "birth_country_code": {
                    "type": "string"
                },
                "citizenship_code": {
                    "type": "string"
                },
                "doc_district_code": {
                    "type": "string"
                },
                "doc_number": {
                    "type": "string"
                },
                "doc_region_code": {
                    "type": "string"
                },
                "doc_series": {
                    "type": "string"
                },
                "domicile_country": {
                    "type": "string"
                },
                "domicile_district": {
                    "type": "string"
                },
                "domicile_region": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "first_name_en": {
                    "type": "string"
                },
                "gender": {
                    "type": "string"
                },
                "gni_code": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "last_name_en": {
                    "type": "string"
                },
                "middle_name": {
                    "type": "string"
                },
                "nationality_code": {
                    "type": "string"
                }
            }
        },
        "rest.reqIdentify": {
            "type": "object",
            "required": [
                "birth_date",
                "consent_approved",
                "ctoken",
                "doc_number",
                "doc_series"
            ],
            "properties": {
                "birth_date": {
                    "type": "string"
                },
                "consent_approved": {
                    "type": "boolean"
                },
                "ctoken": {
                    "type": "string"
                },
                "doc_number": {
                    "type": "string"
                },
                "doc_series": {
                    "type": "string"
                }
            }
        },
        "rest.resFillMissing": {
            "type": "object",
            "properties": {
                "ctoken": {
                    "type": "string"
                },
                "redirect_url": {
                    "type": "string"
                }
            }
        },
        "rest.respInit": {
            "type": "object",
            "properties": {
                "offer": {
                    "$ref": "#/definitions/rest.offer"
                },
                "prospect": {
                    "$ref": "#/definitions/rest.prospect"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "",
	Host:        "",
	BasePath:    "",
	Schemes:     []string{},
	Title:       "Onboarding Physical",
	Description: "",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
