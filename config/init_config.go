package config

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type SubRoutingMap struct {
	Exchange   string
	RoutingKey string
	QueueName  string
}

type Config struct {
	Environment string // development, staging, release

	LogLevel string
	HTTPPort string
	GRPCPort string

	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	PostgresSSLMode  string

	ClientSearchGRPC   string
	NibbdGRPC          string
	SecurityGRPC       string
	SignatureURL       string
	CheckGRPC          string
	ReferencesGRPC     string
	ProductCatalogGRPC string

	AMQPEndpoint string
}

func (c *Config) PostgresURL() string {
	if c.PostgresUser == "" {
		return fmt.Sprintf("host=%s port=%d  dbname=%s sslmode=disable",
			c.PostgresHost,
			c.PostgresPort,
			c.PostgresDatabase)
	}
	if c.PostgresPassword == "" {
		return fmt.Sprintf("host=%s port=%d user=%s  dbname=%s sslmode=disable",
			c.PostgresHost,
			c.PostgresPort,
			c.PostgresUser,
			c.PostgresDatabase)
	}
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		c.PostgresHost,
		c.PostgresPort,
		c.PostgresUser,
		c.PostgresPassword,
		c.PostgresDatabase)
}

// Load configuration into application
func Load() Config {
	if err := godotenv.Load(); // load .env file
	err != nil {
		log.Print("No .env file found")
	}
	config := Config{}

	config.Environment = cast.ToString(env("ENVIRONMENT"))
	config.LogLevel = cast.ToString(env("LOG_LEVEL"))
	config.HTTPPort = cast.ToString(env("HTTP_PORT"))
	config.GRPCPort = cast.ToString(env("GRPC_PORT"))

	config.PostgresHost = cast.ToString(env("POSTGRES_HOST"))
	config.PostgresPort = cast.ToInt(env("POSTGRES_PORT"))
	config.PostgresDatabase = cast.ToString(env("POSTGRES_DATABASE"))
	config.PostgresUser = cast.ToString(env("POSTGRES_USER"))
	config.PostgresPassword = cast.ToString(env("POSTGRES_PASSWORD"))
	config.PostgresSSLMode = cast.ToString(env("POSTGRES_SSLMODE"))

	// ClientSearchGRPC
	config.ClientSearchGRPC = cast.ToString(env("GRPC_CLIENT_SEARCH"))
	// security service
	config.NibbdGRPC = cast.ToString(env("GRPC_NIBBD"))
	// security service
	config.SecurityGRPC = cast.ToString(env("GRPC_SECURITY"))
	// check service
	config.CheckGRPC = cast.ToString(env("GRPC_CHECK"))
	// reference service
	config.ReferencesGRPC = cast.ToString(env("GRPC_REFERENCES"))
	// Product catalog service
	config.ProductCatalogGRPC = cast.ToString(env("GRPC_PRODUCT_CATALOG"))
	// Signature service url
	config.SignatureURL = cast.ToString(env("REST_SIGNATURE"))
	config.AMQPEndpoint = cast.ToString(env("AMQP_URL"))
	return config
}

func env(key string) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	panic(fmt.Sprintf("Config ENV: %v is Missing", key))
}
