CREATE TABLE consents (
    id uuid PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    prospect_id uuid NOT NULL,
    sign_id varchar,
    offer_code VARCHAR,
    offer_version_code VARCHAR,
    accepted_at timestamptz,
    created_at timestamptz
)