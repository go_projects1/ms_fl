CREATE TABLE addresses
(
    id            uuid NOT NULL DEFAULT uuid_generate_v4() PRIMARY KEY,
    profile_id    uuid NOT NULL,
    type          smallint,
    address       VARCHAR,
    registered_at timestamptz,
    country_code  char(3),
    region_code   char(2),
    district_code char(3),
    block         varchar,
    flat          varchar,
    house         varchar,
    place_desc    varchar,
    street_desc   varchar,
    kadastr       varchar,

    created_at timestamptz NOT NULL DEFAULT now(),

    FOREIGN KEY (profile_id)
        REFERENCES profiles(id)
        ON DELETE CASCADE
);