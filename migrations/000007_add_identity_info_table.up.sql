ALTER TABLE prospects
    ADD COLUMN pinfl varchar;
ALTER TABLE prospects
    ADD COLUMN doc_series varchar;
ALTER TABLE prospects
    ADD COLUMN doc_number varchar;
ALTER TABLE prospects
    ADD COLUMN birth_date timestamptz;
ALTER TABLE prospects
    ADD COLUMN approved_at timestamptz;
ALTER TABLE prospects
    ADD COLUMN iabs_client_code varchar;