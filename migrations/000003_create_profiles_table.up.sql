CREATE TABLE profiles (
    id uuid NOT NULL DEFAULT uuid_generate_v4() PRIMARY KEY ,
    source smallint DEFAULT 0,
    prospect_id uuid NOT NULL,
    first_name varchar(255),
    last_name varchar(255),
    middle_name varchar(255),
    first_name_en varchar(255),
    last_name_en varchar(255),
    birth_place varchar(255),
    birth_date DATE,
    birth_country_code char(3),
    gender char(1),
    tin char(9),
    pinfl char(14),
    inps char(14),
    nationality_code char(3),
    citizenship_code char(3),
    tin_registration_date DATE,
    tin_registration_gni varchar,
    subject_state varchar(1),

    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now(),

    doc_type char(1),
    doc_series varchar(3),
    doc_number varchar,
    doc_expiry_date timestamptz,
    doc_issue_date timestamptz,
    doc_issue_org_code varchar(10),
    doc_issue_org_desc varchar,
    doc_region_code char(2),
    doc_district_code char(3),

    FOREIGN KEY (prospect_id)
       REFERENCES "prospects"(id)
)