CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "prospects" (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    state smallint default 0,
    iabs_client_id varchar(12),
    rs_client_id varchar(12),
    is_affiliated bool,
    is_terrorist bool,
    is_profile_complete bool,
    verified_at timestamptz,
    checked_at timestamptz,
    is_consent_needed bool default TRUE,
    created_at timestamptz default now(),
    updated_at timestamptz default now()
)