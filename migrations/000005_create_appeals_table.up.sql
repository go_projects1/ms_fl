CREATE TABLE appeals(
    id uuid PRIMARY KEY NOT NULL  DEFAULT uuid_generate_v4(),
    product_id VARCHAR,
    prospect_id uuid NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),

    FOREIGN KEY (prospect_id)
        REFERENCES prospects(id)
);

