ALTER TABLE prospects
    DROP COLUMN pinfl ;
ALTER TABLE prospects
    DROP COLUMN doc_series;
ALTER TABLE prospects
    DROP COLUMN doc_number;
ALTER TABLE prospects
    DROP COLUMN birth_date;
ALTER TABLE prospects
    DROP COLUMN approved_at;
ALTER TABLE prospects
    DROP COLUMN iabs_client_code;