CREATE TABLE contacts (
  id uuid NOT NULL DEFAULT uuid_generate_v4() PRIMARY KEY ,
  prospect_id uuid NOT NULL ,
  value varchar,
  type smallint default 0,
  verified_at timestamptz,
  created_at timestamptz NOT NULL DEFAULT now(),
  FOREIGN KEY (prospect_id)
    REFERENCES "prospects"(id)
    ON DELETE CASCADE
)