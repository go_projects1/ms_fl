package entities

import "time"

type Document struct {
	DocType         string
	DocSeries       string
	DocNumber       string
	DocExpiryDate   *time.Time
	DocIssueDate    *time.Time
	DocIssueOrgCode string
	DocIssueOrgDesc string
	DocRegionCode   string
	DocDistrictCode string
}

func (d Document) Merge(d2 Document, inPriority bool) Document {
	if d2.DocSeries != "" && d2.DocNumber != "" && d.DocSeries != "" && d.DocNumber != "" &&
		(d.DocSeries != d2.DocSeries || d.DocNumber != d2.DocNumber) {
		if inPriority {
			return d2
		} else {
			return d
		}
	}
	doc := d
	if d2.DocType != "" && (inPriority || d.DocType == "") {
		doc.DocType = d2.DocType
	}
	if d2.DocSeries != "" && (inPriority || d.DocSeries == "") {
		doc.DocSeries = d2.DocSeries
	}
	if d2.DocNumber != "" && (inPriority || d.DocNumber == "") {
		doc.DocNumber = d2.DocNumber
	}
	if d2.DocExpiryDate != nil && (inPriority || d.DocExpiryDate == nil) {
		doc.DocExpiryDate = d2.DocExpiryDate
	}
	if d2.DocIssueDate != nil && (inPriority || d.DocIssueDate == nil) {
		doc.DocIssueDate = d2.DocIssueDate
	}
	if d2.DocIssueOrgCode != "" && (inPriority || d.DocIssueOrgCode == "") {
		doc.DocIssueOrgCode = d2.DocIssueOrgCode
	}
	if d2.DocIssueOrgDesc != "" && (inPriority || d.DocIssueOrgDesc == "") {
		doc.DocIssueOrgDesc = d2.DocIssueOrgDesc
	}
	if d2.DocRegionCode != "" && (inPriority || d.DocRegionCode == "") {
		doc.DocRegionCode = d2.DocRegionCode
	}
	if d2.DocDistrictCode != "" && (inPriority || d.DocDistrictCode == "") {
		doc.DocDistrictCode = d2.DocDistrictCode
	}
	return doc
}
