package entities

import "time"

type IdentityData struct {
	PINFL          string
	DocSeries      string
	DocNumber      string
	BirthDate      *time.Time
	IABSClientID   string
	IABSClientCode string
}
