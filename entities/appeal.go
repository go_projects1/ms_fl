package entities

import "time"

type Appeal struct {
	ID         string
	ProspectID string
	ProductID  string
	CreatedAt  time.Time
}
