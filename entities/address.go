package entities

import "time"

const (
	// AddressTypeDomicile - Адрес прописки
	AddressTypeDomicile = iota
	// AddressTypeTemp - Временный адрес проживания
	AddressTypeTemp
	// AddressTypeWork - Рабочий адрес
	AddressTypeWork
)

type Address struct {
	ID           string
	ProfileID    string
	Type         int8
	Address      string // *
	RegisteredAt *time.Time

	CountryCode  string // *
	RegionCode   string // *
	DistrictCode string // *

	Block      string
	Flat       string
	House      string
	PlaceDesc  string
	StreetDesc string
	Kadastr    string
}

func (a Address) Merge(a2 *Address, priorityIn bool) Address {
	address := a
	if a2 == nil {
		return a
	}
	if a2.Address != "" && (priorityIn || a.Address == "") {
		address.Address = a2.Address
	}
	if a2.RegisteredAt != nil && (priorityIn || a.RegisteredAt == nil) {
		address.RegisteredAt = a2.RegisteredAt
	}
	if a2.CountryCode != "" && (priorityIn || a.CountryCode == "") {
		address.CountryCode = a2.CountryCode
	}
	if a2.RegionCode != "" && (priorityIn || a.RegionCode == "") {
		address.RegionCode = a2.RegionCode
	}
	if a2.DistrictCode != "" && (priorityIn || a.DistrictCode == "") {
		address.DistrictCode = a2.DistrictCode
	}
	if a2.Block != "" && (priorityIn || a.Block == "") {
		address.Block = a2.Block
	}
	if a2.Flat != "" && (priorityIn || a.Flat == "") {
		address.Flat = a2.Flat
	}
	if a2.House != "" && (priorityIn || a.House == "") {
		address.House = a2.House
	}
	if a2.PlaceDesc != "" && (priorityIn || a.PlaceDesc == "") {
		address.PlaceDesc = a2.PlaceDesc
	}
	if a2.StreetDesc != "" && (priorityIn || a.StreetDesc == "") {
		address.StreetDesc = a2.StreetDesc
	}
	if a2.Kadastr != "" && (priorityIn || a.Kadastr == "") {
		address.Kadastr = a2.Kadastr
	}
	return address
}
