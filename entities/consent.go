package entities

import (
	"dbo/onboarding/errs"
	"time"
)

// ConsentCode - Код документа в сервисе справочников
const ConsentCode = "00"

type Consent struct {
	ID               string
	ProspectID       string
	SignID           string
	OfferCode        string
	OfferVersionCode string
	AcceptedAt       *time.Time
	CreatedAt        time.Time
}

func (c *Consent) IsAccepted() bool {
	return c.AcceptedAt != nil
}

func (c *Consent) Validate() error {
	if c.ProspectID == "" || c.SignID == "" || c.OfferCode == "" || c.OfferVersionCode == "" {
		return errs.ErrWrongInput
	}
	return nil
}
