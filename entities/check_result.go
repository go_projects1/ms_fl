package entities

type CheckResults interface {
	IsTerrorist() bool
	IsAffiliated() bool
	IsDocActualizationRequired() bool
}
