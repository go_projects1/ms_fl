package entities

type ProfileSpecRaw struct {
	Default     string `json:"default,omitempty"`
	ExactLength int    `json:"exact_length,omitempty"`
	Dictionary  string `json:"dictionary_code"`
}

//
// func (raw ProfileSpecRaw) Values() []string {
// 	res := make([]string, len(raw.Options))
// 	for i, opt := range raw.Options {
// 		res[i] = opt.GetCode()
// 	}
// 	return res
// }

type ProfileSpec map[string]ProfileSpecRaw
