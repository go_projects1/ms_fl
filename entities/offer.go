package entities

// PublicOfferCode ...

type Localized struct {
	Ru string
	En string
	Uz string
}

type Offer struct {
	Code              string
	LatestVersionCode string
	ShouldBeSigned    bool
	SignatureType     string
	Name              string
	Content           string
}
