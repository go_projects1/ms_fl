package entities

import codes "gitlab.hamkorbank.uz/libs/protos/v2/reference"

const (
	RefDirectoryRegion          = codes.RefID_Region
	RefDirectoryDistrict        = codes.RefID_District
	RefDirectoryCountryCode     = codes.RefID_Countries
	RefDirectoryNationalityCode = codes.RefID_Nationality
	RefDirectoryCitizenshipCode = codes.RefID_Countries
)

type Dictionaries interface {
	Get(id codes.RefID) []ReferenceRecord
	Codes(id codes.RefID) []string
	Validate(id codes.RefID, code string) bool
}

type Translation interface {
	GetRu() string
	GetUz() string
	GetEn() string
}

type ReferenceRecord interface {
	GetCode() string
	GetDescription() Translation
	GetExternalCodes() map[string]string
}
