package entities

import "time"

const (
	// ProspectStateContactLead - номер телефона является ЛИДом
	ProspectStateContactLead = iota
	// ProspectStateContactVerified - Номер телефона подтвержден
	ProspectStateContactVerified
	// ProspectStateIdentityFilled - Данные по проспекту сохранены
	ProspectStateIdentityFilled
	// ProspectStateIdentified - Данные по проспекту сохранены
	ProspectStateIdentified
	// ProspectStateComplete - Существует полная карточка Проспекта
	ProspectStateComplete
)

type Prospect struct {
	IdentityData
	ID         string
	State      int8
	RSClientID string

	IsAffiliated bool
	IsTerrorist  bool
	// IsProfileComplete существуют ли ВСЕ обязательные данные в профилях, для создания клиента в учетных системах
	IsProfileComplete bool
	IsConsentNeeded   bool

	// VerifiedAt дата когда проспект был верифицирован сотрудником банка или онлайн
	VerifiedAt *time.Time
	// CheckedAt дата последних проверок по черным списках
	CheckedAt *time.Time
	// ApprovedAt Дата когда проспект подтвердил дул
	ApprovedAt *time.Time
	CreatedAt  time.Time
	UpdatedAt  time.Time

	Contacts    []Contact
	Profiles    Profiles
	FullProfile *Profile
}

func (p *Prospect) IsClient() bool {
	return p.IABSClientID != ""
}

func (p *Prospect) NeedsToSignOffer() bool {
	return false
}
