package entities

import (
	errs "dbo/onboarding/errs"
	h "dbo/onboarding/helpers"
	"errors"
	"fmt"
	"time"
)

const (
	ProfileSourceLocal = iota
	ProfileSourceUserInput
	ProfileSourceIABS
	ProfileSourceNIBBD
)

type Profile struct {
	ID         string
	Source     int8
	ProspectID string

	FirstName        string
	LastName         string
	MiddleName       string
	FirstNameEn      string
	LastNameEn       string
	BirthPlace       string
	BirthDate        *time.Time
	BirthCountryCode string
	Gender           string

	TIN                 string
	PINFL               string
	INPS                string
	NationalityCode     string
	CitizenshipCode     string
	TINRegistrationDate *time.Time
	TINRegistrationGNI  string
	SubjectState        string

	CreatedAt time.Time
	UpdatedAt time.Time

	addressesLoaded bool
	addresses       []Address

	Document

	IABSClientID   string
	IABSClientCode string
}

func (p *Profile) Addresses() []Address {
	return p.addresses
}
func (p *Profile) SetAddresses(addr []Address) {
	p.addresses = addr
	p.addressesLoaded = true
}

func (p *Profile) Address(t int8) (Address, error) {
	var address Address
	if !p.addressesLoaded {
		return address, errs.Errf(errs.ErrEntityRelationNotLoaded, "entity:%v; relation: %v", "Profile", "Address")
	}
	for i := 0; i < len(p.addresses); i++ {
		if p.addresses[i].Type == t {
			return p.addresses[i], nil
		}
	}
	return address, errs.Errf(errs.ErrNotFound, "entity:%v; relation: %v; type: %v", "Profile", "Address", t)
}

type Profiles []Profile

func (pp Profiles) Sort() Profiles {
	for i := 0; i < len(pp); i++ {
		for j := i; j < len(pp); j++ {
			if pp[j].Source >= pp[i].Source {
				pp[i], pp[j] = pp[j], pp[i]
			}
		}
	}
	return pp
}

func (pp Profiles) GetFirstBySource(source int8) *Profile {
	for i := range pp {
		if pp[i].Source == source {
			return &pp[i]
		}
	}
	return nil
}

func (pp Profiles) HasActualSource() bool {
	return true
}

func MergeProfiles(profiles Profiles) (Profile, error) {
	var result = Profile{}
	var addresses []Address
	var domicile = Address{
		Type: AddressTypeDomicile,
	}
	var tmp = Address{
		Type: AddressTypeTemp,
	}
	var hasDomicile, hasTmp bool
	if len(profiles) == 0 {
		return result, errs.Errf(errs.ErrWrongInput, "%v no profiles are provided", "MergeProfiles")
	}
	canChangeName := true
	sortedProfiles := profiles.Sort()
	for i := range sortedProfiles {
		prioritized := sortedProfiles[i].Source > result.Source
		if canChangeName && sortedProfiles[i].FirstName != "" &&
			(prioritized || result.FirstName == "") {
			result.FirstName = sortedProfiles[i].FirstName
		}
		if canChangeName && sortedProfiles[i].LastName != "" &&
			(prioritized || result.LastName == "") {
			result.LastName = sortedProfiles[i].LastName
		}
		if canChangeName && sortedProfiles[i].MiddleName != "" &&
			(prioritized || result.MiddleName == "") {
			result.MiddleName = sortedProfiles[i].MiddleName
		}
		if canChangeName && sortedProfiles[i].LastNameEn != "" &&
			(prioritized || result.LastNameEn == "") {
			result.LastNameEn = sortedProfiles[i].LastNameEn
		}
		if canChangeName && sortedProfiles[i].FirstNameEn != "" &&
			(prioritized || result.FirstNameEn == "") {
			result.FirstNameEn = sortedProfiles[i].FirstNameEn
		}
		if sortedProfiles[i].BirthPlace != "" &&
			(prioritized || result.BirthPlace == "") {
			result.BirthPlace = sortedProfiles[i].BirthPlace
		}
		if sortedProfiles[i].BirthDate != nil && (prioritized || result.BirthDate == nil) {
			result.BirthDate = sortedProfiles[i].BirthDate
		}
		if sortedProfiles[i].BirthCountryCode != "" &&
			(prioritized || result.BirthCountryCode == "") {
			result.BirthCountryCode = sortedProfiles[i].BirthCountryCode
		}
		if sortedProfiles[i].Gender != "" && (prioritized || result.Gender == "") {
			result.Gender = sortedProfiles[i].Gender
		}
		if sortedProfiles[i].TIN != "" && (prioritized || result.TIN == "") {
			result.TIN = sortedProfiles[i].TIN
		}
		if sortedProfiles[i].PINFL != "" && (prioritized || result.PINFL == "") {
			result.PINFL = sortedProfiles[i].PINFL
		}
		if sortedProfiles[i].INPS != "" && (prioritized || result.INPS == "") {
			result.INPS = sortedProfiles[i].INPS
		}
		if sortedProfiles[i].NationalityCode != "" && (prioritized || result.NationalityCode == "") {
			result.NationalityCode = sortedProfiles[i].NationalityCode
		}
		if sortedProfiles[i].CitizenshipCode != "" && (prioritized || result.CitizenshipCode == "") {
			result.CitizenshipCode = sortedProfiles[i].CitizenshipCode
		}
		if sortedProfiles[i].TINRegistrationDate != nil &&
			(prioritized || result.TINRegistrationDate == nil) {
			result.TINRegistrationDate = sortedProfiles[i].TINRegistrationDate
		}
		if sortedProfiles[i].TINRegistrationGNI != "" &&
			(prioritized || result.TINRegistrationGNI == "") {
			result.TINRegistrationGNI = sortedProfiles[i].TINRegistrationGNI
		}
		if sortedProfiles[i].SubjectState != "" && (prioritized || result.SubjectState == "") {
			result.SubjectState = sortedProfiles[i].SubjectState
		}

		if sortedProfiles[i].IABSClientID != "" && (prioritized || result.IABSClientID == "") {
			result.IABSClientID = sortedProfiles[i].IABSClientID
		}
		if sortedProfiles[i].IABSClientCode != "" && (prioritized || result.IABSClientCode == "") {
			result.IABSClientCode = sortedProfiles[i].IABSClientCode
		}
		domicile2, err := sortedProfiles[i].Address(AddressTypeDomicile)
		switch {
		case err == nil:
			domicile = domicile.Merge(&domicile2, prioritized)
			hasDomicile = true
		case errors.Is(err, errs.ErrNotFound):
			break
		default:
			return result, err
		}
		tmp2, err := sortedProfiles[i].Address(AddressTypeTemp)
		switch {
		case err == nil:
			tmp = domicile.Merge(&tmp2, prioritized)
			hasTmp = true
		case errors.Is(err, errs.ErrNotFound):
			break
		default:
			return result, err
		}
		result.Document = result.Document.Merge(sortedProfiles[i].Document, prioritized)

		// NamesCan not be changed if IABS
		if sortedProfiles[i].Source == ProfileSourceIABS {
			canChangeName = false
		}
	}
	if hasTmp {
		addresses = append(addresses, tmp)
	}
	if hasDomicile {
		addresses = append(addresses, domicile)
	}
	result.SetAddresses(addresses)
	return result, nil
}

func (p Profile) Validate(dicts Dictionaries) (Profile, ProfileSpec, error) {
	spec := ProfileSpec{}

	if h.IsEmpty(p.FirstName) {
		spec["first_name"] = ProfileSpecRaw{Default: ""}
	}
	if h.IsEmpty(p.LastName) {
		spec["last_name"] = ProfileSpecRaw{Default: ""}
	}
	if h.IsEmpty(p.MiddleName) {
		spec["middle_name"] = ProfileSpecRaw{Default: ""}
	}
	if h.IsEmpty(p.FirstNameEn) {
		spec["first_name_en"] = ProfileSpecRaw{Default: ""}
	}
	if h.IsEmpty(p.LastNameEn) {
		spec["last_name_en"] = ProfileSpecRaw{Default: ""}
	}
	if p.BirthDate == nil {
		spec["birth_date"] = ProfileSpecRaw{Default: ""}
	}
	if ok := dicts.Validate(RefDirectoryCountryCode, p.BirthCountryCode); !ok {
		spec["birth_country_code"] = ProfileSpecRaw{
			Default:    "",
			Dictionary: fmt.Sprintf("%v", RefDirectoryCountryCode),
		}
		p.BirthCountryCode = ""
	}
	if !h.Contains([]string{"1", "2"}, p.Gender) {
		spec["gender"] = ProfileSpecRaw{Default: ""}
		p.Gender = ""
	}

	if !h.Length(p.PINFL, 14) {
		spec["pinfl"] = ProfileSpecRaw{Default: "", ExactLength: 14}
		p.PINFL = ""
	}

	if ok := dicts.Validate(RefDirectoryNationalityCode, p.NationalityCode); !ok {
		spec["nationality_code"] = ProfileSpecRaw{Dictionary: dict(RefDirectoryNationalityCode)}
		p.NationalityCode = ""
	}

	if ok := dicts.Validate(RefDirectoryCountryCode, p.CitizenshipCode); !ok {
		spec["citizenship_code"] = ProfileSpecRaw{Dictionary: dict(RefDirectoryCountryCode)}
		p.CitizenshipCode = ""
	}
	if h.IsEmpty(p.TINRegistrationGNI) {
		spec["gni_code"] = ProfileSpecRaw{}
		p.TINRegistrationGNI = ""
	}
	if ok := dicts.Validate(RefDirectoryRegion, p.DocRegionCode); !ok {
		spec["doc_region_code"] = ProfileSpecRaw{Dictionary: dict(RefDirectoryRegion)}
		p.DocRegionCode = ""
	}
	if ok := dicts.Validate(RefDirectoryDistrict, p.DocDistrictCode); !ok {
		spec["doc_district_code"] = ProfileSpecRaw{
			Default:    "",
			Dictionary: dict(RefDirectoryDistrict),
		}
		p.DocDistrictCode = ""
	}
	if h.IsEmpty(p.DocSeries) {
		spec["doc_series"] = ProfileSpecRaw{
			Default:     "",
			ExactLength: 2,
		}
		p.DocSeries = ""
	}
	if h.IsEmpty(p.DocNumber) {
		spec["doc_number"] = ProfileSpecRaw{
			Default:     "",
			ExactLength: 7,
		}
		p.DocNumber = ""
	}
	address, err := p.Address(AddressTypeDomicile)
	if err != nil && errors.Is(err, errs.ErrEntityRelationNotLoaded) {
		return p, spec, errs.Errf(err, err.Error())
	}

	ValidateAddress(dicts, address, spec)

	return p, spec, nil
}

func ValidateAddress(dicts Dictionaries, address Address, spec map[string]ProfileSpecRaw) {
	if ok := dicts.Validate(RefDirectoryDistrict, address.DistrictCode); !ok {
		spec["domicile_district"] = ProfileSpecRaw{
			Default:    "",
			Dictionary: dict(RefDirectoryDistrict),
		}
		address.DistrictCode = ""
	}
	if ok := dicts.Validate(RefDirectoryRegion, address.RegionCode); !ok {
		spec["domicile_region"] = ProfileSpecRaw{
			Default:    "",
			Dictionary: dict(RefDirectoryRegion),
		}
		address.RegionCode = ""
	}
	if ok := dicts.Validate(RefDirectoryCountryCode, address.CountryCode); !ok {
		spec["domicile_country"] = ProfileSpecRaw{
			Default:    "",
			Dictionary: dict(RefDirectoryCountryCode),
		}
		address.CountryCode = ""
	}
}

func dict(code interface{}) string {
	return fmt.Sprintf("%v", code)
}
