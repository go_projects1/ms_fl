package entities

import "time"

const (
	ContactTypePhoneMobile = iota
	ContactTypePhoneHome
	ContactTypePhoneWorking
	ContactTypeEmail
	ContactTypePhoneMobileMB
)

type Contact struct {
	ID         string
	ProspectID string
	Value      string
	Type       int8
	VerifiedAt *time.Time
	CreatedAt  time.Time
}

func (c Contact) IsVerified() bool {
	return c.VerifiedAt != nil
}
