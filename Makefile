
.PHONY: all build run build-image push-image swag-init dep build clean test coverage coverhtml lint-go lint-yaml

run: ## run application
	go run cmd/main.go

swag-init:
	swag init -g cmd/main.go -o api/docs
