package helpers

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func IsEmpty(s string) bool {
	return s == ""
}

func Length(s string, count int) bool {
	return len(s) == count
}

func StrArrIncludes(stack []string, needle string) bool {
	for i := 0; i < len(stack); i++ {
		if needle == stack[i] {
			return true
		}
	}
	return false
}
