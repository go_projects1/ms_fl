package helpers

import (
	"time"
)

const TimeStringTemplate = "2006-01-02T15:04:05Z07"
const DateStringTemplate = "2006-01-02"

func ParseTime(timestr string) (*time.Time, error) {
	if len(timestr) == len("2006-01-02T15:04:05") {
		timestr += "Z"
	}
	ts, err := time.Parse(TimeStringTemplate, timestr)
	if err != nil {
		return nil, err
	}
	ts = ts.UTC()
	return &ts, nil
}

func ParseDate(timestr string) (*time.Time, error) {
	ts, err := time.Parse(DateStringTemplate, timestr)
	if err != nil {
		return nil, err
	}
	return &ts, nil
}
