package helpers

import (
	"reflect"
	"testing"
	"time"
)

func TestParseTime(t *testing.T) {
	tests := []struct {
		name    string
		args    string
		want    *time.Time
		wantErr bool
	}{
		{
			"Success",
			"2019-08-24T14:15:22Z",
			func() *time.Time {
				ts := time.Date(2019, 8, 24, 14, 15, 22, 0, time.UTC)
				return &ts
			}(),
			false,
		}, {
			"Success",
			"2019-08-24T14:15:22",
			func() *time.Time {
				ts := time.Date(2019, 8, 24, 14, 15, 22, 0, time.UTC)
				return &ts
			}(),
			false,
		}, {
			"Success",
			"2019-08-24T14:15:22+04",
			func() *time.Time {
				ts := time.Date(2019, 8, 24, 10, 15, 22, 0, time.UTC)
				return &ts
			}(),
			false,
		}, {
			"Success",
			"2019-08-24T14:15:22-04",
			func() *time.Time {
				ts := time.Date(2019, 8, 24, 18, 15, 22, 0, time.UTC)
				return &ts
			}(),
			false,
		}, {
			"Error",
			"2019-08-24T14:15:220Z",
			nil,
			true,
		}, {
			"Error",
			"2019-08-24T",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseTime(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseTime() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseDate(t *testing.T) {
	tests := []struct {
		name    string
		args    string
		want    *time.Time
		wantErr bool
	}{
		{
			"Success",
			"2019-08-24",
			func() *time.Time {
				ts := time.Date(2019, 8, 24, 0, 0, 0, 0, time.UTC)
				return &ts
			}(),
			false,
		}, {
			"Error",
			"2019-02-30",
			nil,
			true,
		}, {
			"Error",
			"24.08.2019",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseDate(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseDate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseDate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
