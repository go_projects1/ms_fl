package status

var (
	NoError                       = 0
	ErrorCodeValidation           = -10
	ErrorCodeValidationDateFormat = -12
	ErrorCodeDB                   = -30
	ErrorCodeRemoteDBO            = -40
	ErrorCodeRemoteESB            = -45
	ErrorCodeRemoteCRM            = -50
	ErrorCodeRemoteOther          = -55
	ErrorCodeRMQ                  = -60
)

var (
	Success = "Success"
	Failure = "Failure"
)
