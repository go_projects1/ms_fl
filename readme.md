# Сервис Онбоардинга ФЛ

## Задачи
- Хранение Проспекта/Клиента
- Хранение Досье (Профили) Проспекта/клиента
- Хранение Фактов согласий СОПД
- Хранение Контактов Проспекта/Клиента
- Актуализация и Создание клиентов в учетных системах банка
- BFF "Онбоардинг ФЛ для пользователя канала"
- BFF "Онбоардинг ФЛ для операциониста из канала СРМ"

## Порты для взаимодействия
- MQ 
- gRPC - синхронные операции между сервисами
- REST (BFF) - Синхронные операции для взаимодействие с фронтом

## Use Cases
UseCase - Бизнес логика приложения [подробнее про Архитектуру приложения](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
1. Сохранение уже принятого СОПД - usecases.SaveConsent. Порты: MQ, (BFF) rest 
2. Поиск и создание проспекта по ДУЛ - usecases.Identify. Порты: (BFF) rest 
3. Вызов проверок и сохранение результат проверов - usecases.CheckProspect. Порты: internal
4. Дозаполнение полей недостающих в карточке клиентов и подтверждение со стороны пользователя - usecases.FillMissingAndApprove. Порты: (BFF) rest
5. Получение/Создание проспекта по паре IABSClientID/Контакт - usecases.ProspectByIABS. Порты: grpc
6. Получение/Создание проспекта по Контакту - usecases.ProspectByPhone. Порты: grpc (e.g. HamkorStoreBack), mq (e.g. MobileBankingBackend) ___В случае для мобильного банка Также сохраняет ID проспекта в AuthAPI/WSO2IS___
### Стандартный вызов UseCase из кода
```go
uc:=usecases.NewSaveConsent(...) // Создаем 
if err:=uc.Execute();err!=nil{ // Запускаем 
	return err
}
result:= uc.Result() // Получаем результат в случае успешного прохождения по сценарию
```

## Сущности
| Название | Описание | Таблица |
|---|---|---|
|entities.Prospect| Проспект/ЛИД/Клиент в зависимости от статуса| prospects|
|entities.Contact |Контакт клиента (Тип/Значение/Подтвержден?) | contacts|
|entities.Consent | Факт согласия с СОПД     | consents|
|entities.Appeal  | Обращение - ЛИД продукта | appeals |
|entities.Profile | Профиль - Анкета с типом источника данных | profiles|
|entities.Address | Адрес - Тип/Регион/Район/Значение | addresses
|entities.Document| Документ профиля | profiles (nested)|
|entities.ReferenceResult| запись в сервисе справочников | srv.reference-service |
|entities.IdentityData | Идентификаторы ФЛ в системах | prospects (nested)| 
|entities.Offer   | Оферта - запись оферты в сервисе справочников |srv.reference-service|

## Коды ошибок (grpc)
| Название | Описание  |
|---|---|
|-500| Внутренняя ошибка   |
|-404| Сущность не найдена |
| 