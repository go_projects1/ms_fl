package services

import (
	"dbo/onboarding/contracts"
)

type Container interface {
	Security() Security
	Signature() contracts.ISignatureService
	References() contracts.IReferences
	Offers() contracts.IMasterOfferSource
	Clients() contracts.IProfileSearcher
	NIBBD() contracts.IProfileSearcher
	Check() contracts.IChecker
}

type Security interface {
	DecodeToken(token string) (map[string]interface{}, error)
	EncodeData(map[string]interface{}) (string, error)
}
