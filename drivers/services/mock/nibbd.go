package mock

import (
	"context"
	"dbo/onboarding/entities"
	"time"
)

type nibbd struct {
}

func (n *nibbd) Search(ctx context.Context, data entities.IdentityData) (entities.Profile, error) {
	bd, _ := time.Parse("2006-01-02", "1993-11-10")
	expiry, _ := time.Parse("2006-01-02", "2023-06-23")
	issue, _ := time.Parse("2006-01-02", "2013-06-23")
	now := time.Now()
	return entities.Profile{
		Source:              entities.ProfileSourceNIBBD,
		FirstName:           "MARK",
		LastName:            "SOY",
		MiddleName:          "DMITRIYEVICH",
		FirstNameEn:         "MARK",
		LastNameEn:          "TSOY",
		BirthPlace:          "TOSHKENT",
		BirthDate:           &bd,
		BirthCountryCode:    "860",
		Gender:              "1",
		TIN:                 "000000000",
		PINFL:               "31011930192484",
		INPS:                "31011930192484",
		NationalityCode:     "054",
		CitizenshipCode:     "182",
		TINRegistrationDate: &now,
		TINRegistrationGNI:  "2603",
		SubjectState:        "1",
		Document: entities.Document{
			DocType:         "6",
			DocSeries:       "AA",
			DocNumber:       "1918563",
			DocExpiryDate:   &expiry,
			DocIssueDate:    &issue,
			DocIssueOrgCode: "26266",
			DocIssueOrgDesc: "",
			DocRegionCode:   "26",
			DocDistrictCode: "266",
		},
	}, nil
}
