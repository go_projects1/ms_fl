package mock

import (
	"context"
	"dbo/onboarding/entities"
	"time"
)

type clients struct {
}

func (c *clients) GetByID(ctx context.Context, iabsClientID string) (entities.Profile, error) {
	if iabsClientID == "" {
		return entities.Profile{}, Err
	}
	bd, _ := time.Parse("2006-01-02", "1993-11-10")
	now := time.Now()
	return entities.Profile{
		ID:                  iabsClientID,
		Source:              entities.ProfileSourceIABS,
		FirstName:           "MARK",
		LastName:            "SOY",
		MiddleName:          "DMITRIYEVICH",
		FirstNameEn:         "",
		LastNameEn:          "",
		BirthPlace:          "",
		BirthDate:           &bd,
		BirthCountryCode:    "860",
		Gender:              "1",
		TIN:                 "111111111",
		PINFL:               "31011930192484",
		INPS:                "31011930192484",
		NationalityCode:     "054",
		CitizenshipCode:     "860",
		TINRegistrationDate: nil,
		TINRegistrationGNI:  "",
		SubjectState:        "",
		Document: entities.Document{
			DocType:         "06",
			DocSeries:       "AA",
			DocNumber:       "88888888",
			DocExpiryDate:   &now,
			DocIssueDate:    &bd,
			DocIssueOrgCode: "",
			DocIssueOrgDesc: "ЮНУСАБАДСКИЙ РУВД ГОРОДА ТАШКЕНТА",
			DocRegionCode:   "26",
			DocDistrictCode: "",
		},
	}, nil
}

func (c *clients) Search(ctx context.Context, data entities.IdentityData) (entities.Profile, error) {
	return c.GetByID(ctx, "5230208")
}
