package mock

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/drivers/services"
	"dbo/onboarding/entities"
	"errors"
)

var Err = errors.New("mock error")

type Container struct {
	clients *clients
	nibbd   *nibbd
	offer   *offer
}

func (c Container) Check() contracts.IChecker {
	panic("implement me")
}

func New() Container {
	return Container{
		clients: &clients{},
		nibbd:   &nibbd{},
		offer:   &offer{},
	}
}

func (c Container) Security() services.Security {
	panic("implement me")
}

func (c Container) Signature() contracts.ISignatureService {
	panic("implement me")
}

func (c Container) References() contracts.IReferences {
	panic("implement me")
}

func (c Container) Offers() contracts.IMasterOfferSource {
	return c.offer
}

func (c Container) Clients() contracts.IProfileSearcher {
	return c.clients
}

func (c Container) NIBBD() contracts.IProfileSearcher {
	return c.nibbd
}

type offer struct {
}

func (o *offer) GetOffer(ctx context.Context, consentCode, lang string) (entities.Offer, error) {
	return entities.Offer{
		Code:              entities.ConsentCode,
		LatestVersionCode: "22102021",
		Name:              "Публичная оферта",
	}, nil
}
