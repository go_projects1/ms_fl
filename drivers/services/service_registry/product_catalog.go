package service_registry

import (
	"context"
	"dbo/onboarding/pkg/logger"

	proto "gitlab.hamkorbank.uz/libs/protos/genproto/product_catalog"
	"google.golang.org/grpc"
)

type ProductsCatalog struct {
	grpcClient proto.ProductCatalogClient
	logger.Logger
}

func NewProductCatalog(host string, l logger.Logger) *ProductsCatalog {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &ProductsCatalog{
		grpcClient: proto.NewProductCatalogClient(conn),
		Logger:     l,
	}
}

func (c *ProductsCatalog) GetProductURL(ctx context.Context, serviceID uint32) string {
	resp, err := c.grpcClient.FindService(ctx, &proto.FindServiceReq{ID: serviceID})
	if err != nil || resp.GetService() == nil {
		return "/hamkor-store"
	}
	return resp.GetService().GetUrl()
}
