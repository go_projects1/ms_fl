package service_registry

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/pkg/logger"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.hamkorbank.uz/libs/protos/v2/nibbd"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var ErrNIBBDConnection = errors.New("NIBBD: connection_failed")
var ErrNIBBDNotFound = errors.New("NIBBD: not_found")
var ErrNIBBDValidation = errors.New("NIBBD: validation failed")

type NIBBD struct {
	grpcClient nibbd.NIBBDClient
	logger.Logger
}

func NewNIBBD(endpoint string, l logger.Logger) *NIBBD {
	c, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		l.Error("error while dialing nibbd grpc: " + err.Error())
		return nil
	}
	client := nibbd.NewNIBBDClient(c)
	return &NIBBD{
		grpcClient: client,
		Logger:     l,
	}
}

func (srv *NIBBD) Search(ctx context.Context, data entities.IdentityData) (entities.Profile, error) {
	var inBirthDate *timestamppb.Timestamp
	if data.BirthDate != nil {
		inBirthDate = timestamppb.New(*data.BirthDate)
	}
	in := nibbd.PhysicalParams{
		DocSeries: data.DocSeries,
		DocNumber: data.DocNumber,
		PINFL:     data.PINFL,
		BirthDate: inBirthDate,
	}
	resp, err := srv.grpcClient.GetPhysical(ctx, &in)
	if err != nil {
		switch {
		case strings.HasPrefix(err.Error(), "[connection_failed]"):
			return entities.Profile{}, ErrNIBBDConnection
		case strings.HasPrefix(err.Error(), "[wrong_request]"):
			return entities.Profile{}, ErrNIBBDValidation
		case strings.HasPrefix(err.Error(), "[person_not_found]"):
			return entities.Profile{}, ErrNIBBDNotFound
		}
		return entities.Profile{}, err
	}
	citizenship := "860"

	p := entities.Profile{
		Source:              entities.ProfileSourceNIBBD,
		FirstName:           resp.FirstName,
		LastName:            resp.LastName,
		MiddleName:          resp.MiddleName,
		FirstNameEn:         resp.FirstNameEn,
		LastNameEn:          resp.LastNameEn,
		BirthPlace:          resp.BirthPlace,
		BirthDate:           pbTimeToTime(resp.GetBirthDate()),
		BirthCountryCode:    resp.GetBirthCountryCode(),
		Gender:              resp.GetGender(),
		TIN:                 resp.GetTIN(),
		PINFL:               resp.GetPINFL(),
		INPS:                resp.GetPINFL(),
		NationalityCode:     resp.GetNationalityCode(),
		CitizenshipCode:     citizenship,
		TINRegistrationDate: pbTimeToTime(resp.GetTINRegisteredAt()),
		TINRegistrationGNI:  resp.GetTINRegistrationGNI(),
		SubjectState:        resp.GetSubjectState(),
	}

	var rCode, dCode string
	if n, err := strconv.Atoi(resp.GetDocIssueOrgCode()); err == nil {
		formatted := fmt.Sprintf("%05d", n)
		rCode = formatted[:2]
		dCode = formatted[2:5]
	}
	srv.Debug(fmt.Sprintf("Region: %v; District: %v;", rCode, dCode))
	d := entities.Document{
		DocType:         resp.GetDocType(),
		DocSeries:       resp.GetDocSeries(),
		DocNumber:       resp.GetDocNumber(),
		DocExpiryDate:   pbTimeToTime(resp.DocExpiryDate),
		DocIssueDate:    pbTimeToTime(resp.DocIssueDate),
		DocIssueOrgCode: resp.GetDocIssueOrgCode(),
		DocIssueOrgDesc: resp.GetDocIssueOrgName(),
		DocRegionCode:   rCode,
		DocDistrictCode: dCode,
	}
	p.Document = d

	addresses := make([]entities.Address, 0)

	if resp.DomicileAddress != nil {
		addresses = append(addresses, transformAddress(resp.DomicileAddress, entities.AddressTypeDomicile))
	}
	if resp.TempAddress != nil {
		addresses = append(addresses, transformAddress(resp.TempAddress, entities.AddressTypeTemp))
	}
	p.SetAddresses(addresses)

	return p, nil
}

func transformAddress(addr *nibbd.Address, addrType int8) entities.Address {
	return entities.Address{
		Type:         addrType,
		Address:      addr.Address,
		RegisteredAt: pbTimeToTime(addr.RegisteredAt),
		CountryCode:  addr.CountryCode,
		RegionCode:   addr.RegionCode,
		DistrictCode: addr.DistrictCode,
		Block:        addr.Block,
		Flat:         addr.Flat,
		House:        addr.House,
		PlaceDesc:    addr.PlaceDesc,
		StreetDesc:   addr.StreetDesc,
		Kadastr:      addr.Kadastr,
	}
}
