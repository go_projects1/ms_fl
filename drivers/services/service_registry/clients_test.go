package service_registry_test

import (
	"context"
	"dbo/onboarding/drivers/services/service_registry"
	"dbo/onboarding/entities"
	"dbo/onboarding/pkg/logger"
	"flag"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var iabsEndpoint = flag.String("iabs_grpc_endpoint", "localhost:7576", "test iabs client")

func iabsClient(t *testing.T) *service_registry.Clients {
	t.Helper()
	l := logger.New("debug", "test_iabs")
	return service_registry.NewIABSClients(*iabsEndpoint, l)
}

func TestIABS_Search(t *testing.T) {
	srv := iabsClient(t)
	type tc struct {
		name    string
		args    entities.IdentityData
		want    entities.Profile
		wantErr bool
		err     error
	}
	tests := make([]tc, 0)

	bd, _ := time.Parse(dateFormat, "1993-11-10")
	gni, _ := time.Parse(dateFormat, "2015-09-25")
	tests = append(tests, tc{
		name: "Success",
		args: entities.IdentityData{
			PINFL: "31011930192484",
		},
		want: entities.Profile{
			Source:              entities.ProfileSourceIABS,
			FirstName:           "MARK",
			LastName:            "SOY",
			MiddleName:          "DMITRIYEVICH",
			FirstNameEn:         "MARK",
			LastNameEn:          "TSOY",
			BirthPlace:          "TOSHKENT",
			BirthDate:           &bd,
			BirthCountryCode:    "860",
			Gender:              "1",
			TIN:                 "548965982",
			PINFL:               "31011930192484",
			INPS:                "31011930192484",
			NationalityCode:     "054",
			CitizenshipCode:     "182",
			TINRegistrationDate: &gni,
			TINRegistrationGNI:  "2603",
			SubjectState:        "1",
			Document:            entities.Document{},
		},
		wantErr: false,
	})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := srv.Search(context.Background(), tt.args)
			want := tt.want
			if tt.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, tt.err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, want.Source, got.Source)
				assert.Equal(t, want.FirstName, got.FirstName)
				assert.Equal(t, want.LastName, got.LastName)
				assert.Equal(t, want.FirstNameEn, got.FirstNameEn)
				assert.Equal(t, want.LastNameEn, got.LastNameEn)
				assert.Equal(t, want.MiddleName, got.MiddleName)
			}
		})
	}
}
