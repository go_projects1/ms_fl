package service_registry

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/helpers"
	"dbo/onboarding/pkg/logger"

	proto "gitlab.hamkorbank.uz/libs/protos/v2/reference"
	"google.golang.org/grpc"
)

type References struct {
	grpcClient proto.ReferenceClient
	logger.Logger
}

func (r *References) GetOffer(ctx context.Context, offerCode, lang string) (entities.Offer, error) {
	var offer entities.Offer
	resp, err := r.grpcClient.GetOffer(ctx, &proto.OfferRequest{
		Lang: lang,
		Code: offerCode,
	})
	if err != nil {
		return entities.Offer{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	switch resp.ErrorCode {
	case proto.ErrCodes_Ok:
		respOffer := resp.GetOffer()
		offer.Name = respOffer.GetName()
		offer.Content = respOffer.GetContent()
		offer.Code = respOffer.GetCode()
		offer.LatestVersionCode = respOffer.GetOfferVersionCode()
		offer.ShouldBeSigned = respOffer.GetShouldBeSigned()
		offer.SignatureType = respOffer.GetSignatureType()
		return offer, nil
	case proto.ErrCodes_ErrValidation:
		return offer, errs.Errf(errs.ErrValidation, "%v: (Code: %v, Lang: %v)", resp.ErrorNote, offerCode, lang)
	case proto.ErrCodes_ErrFatalCode:
		return offer, errs.Errf(errs.ErrSourceInternal, "%v", resp.ErrorNote)
	case proto.ErrCodes_ErrNotFound:
		return offer, errs.Errf(errs.ErrNotFound, "%v: (Code: %v, Lang: %v)", resp.ErrorNote, offerCode, lang)
	default:
		return offer, errs.Errf(errs.ErrSourceInternal, "%v: (Code: %v, Lang: %v)", resp.ErrorNote, offerCode, lang)
	}
}

func newReferences(host string, l logger.Logger) *References {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &References{
		Logger:     l,
		grpcClient: proto.NewReferenceClient(conn),
	}
}

func (r *References) Get(ctx context.Context, codes ...proto.RefID) (entities.Dictionaries, error) {
	resp, err := r.grpcClient.GetReferences(ctx, &proto.GenericRecordsRequest{ReferenceIDs: codes})
	if err != nil {
		return nil, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}
	switch resp.ErrorCode {
	case proto.ErrCodes_Ok:
		data := resp.GetData()
		res := results{items: make(map[proto.RefID][]entities.ReferenceRecord)}
		for _, col := range data {
			records := make([]entities.ReferenceRecord, 0)
			for _, rec := range col.GetRecords() {
				r := row{
					code:     rec.GetCode(),
					desc:     rec.GetDescription(),
					external: rec.GetExternalCodes(),
				}
				records = append(records, r)
			}
			res.items[col.Number] = records
		}
		return &res, nil
	case proto.ErrCodes_ErrValidation:
		return nil, errs.Errf(errs.ErrWrongInput, resp.ErrorNote)
	case proto.ErrCodes_ErrNotFound:
		return nil, errs.Errf(errs.ErrNotFound, resp.ErrorNote)
	case proto.ErrCodes_ErrFatalCode:
		return nil, errs.Errf(errs.ErrSourceInternal, resp.ErrorNote)
	default:
		return nil, errs.Errf(errs.ErrSourceConnectionErr, resp.ErrorNote)
	}
}

type row struct {
	code     string
	desc     *proto.Translations
	external map[string]string
}

func (r row) GetCode() string {
	return r.code
}

func (r row) GetDescription() entities.Translation {
	return r.desc
}

func (r row) GetExternalCodes() map[string]string {
	return r.external
}

type results struct {
	items map[proto.RefID][]entities.ReferenceRecord
	codes map[proto.RefID][]string
}

func (r *results) Validate(id proto.RefID, code string) bool {
	codes := r.Codes(id)
	return helpers.Contains(codes, code)
}

func (r *results) Get(id proto.RefID) []entities.ReferenceRecord {
	rows, ok := r.items[id]
	if !ok {
		return nil
	}
	return rows
}

func (r *results) Codes(id proto.RefID) []string {
	if r.codes == nil {
		r.codes = make(map[proto.RefID][]string)
	}
	codes, ok := r.codes[id]
	if !ok {
		col := r.Get(id)
		codes = make([]string, len(col))
		for i := range col {
			codes = append(codes, col[i].GetCode())
		}
		r.codes[id] = codes
	}
	return codes
}
