package service_registry

import (
	"context"
	"dbo/onboarding/pkg/logger"
	"errors"
	"time"

	proto "gitlab.hamkorbank.uz/libs/protos/genproto/security_service"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/structpb"
)

var ErrSecurityDecode = errors.New("security decode failed")
var ErrSecurityEncode = errors.New("security encode failed")

const requestTimeout = 4 * time.Second

type Security struct {
	grpcClient proto.SecurityClient
	logger.Logger
}

func (s *Security) DecodeToken(token string) (map[string]interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()
	resp, err := s.grpcClient.Extract(ctx, &proto.GenerateTokenResponse{Token: token})
	if err != nil {
		return nil, ErrSecurityDecode
	}
	return resp.Data.AsMap(), nil
}

func (s *Security) EncodeData(m map[string]interface{}) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10)
	defer cancel()
	data, err := structpb.NewStruct(m)
	if err != nil {
		return "", ErrSecurityEncode
	}
	resp, err := s.grpcClient.Generate(ctx, &proto.GenerateTokenRequest{Claims: data})
	if err != nil {
		return "", ErrSecurityEncode
	}
	return resp.Token, nil
}

// newSecurity - initiate security
func newSecurity(host string, log logger.Logger) *Security {
	c, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &Security{grpcClient: proto.NewSecurityClient(c), Logger: log}
}
