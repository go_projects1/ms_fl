package service_registry

import (
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func pbTimeToTime(ts *timestamppb.Timestamp) *time.Time {
	if ts != nil {
		dt := ts.AsTime()
		return &dt
	}
	return nil
}

//
// func timeToPbTime(ts *time.Time) *timestamppb.Timestamp {
// 	if ts != nil {
// 		return timestamppb.New(*ts)
// 	}
// 	return nil
// }

func timeToString(ts *time.Time) string {
	if ts != nil {
		return ts.Format("2006-01-02T15:04:05")
	}
	return ""
}
