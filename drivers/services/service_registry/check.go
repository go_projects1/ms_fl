package service_registry

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"

	"gitlab.hamkorbank.uz/libs/protos/genproto/checkgrpc"
	"google.golang.org/grpc"
)

type CheckService struct {
	grpcClient checkgrpc.CheckClient
	logger.Logger
}

func newCheck(host string, l logger.Logger) *CheckService {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &CheckService{
		grpcClient: checkgrpc.NewCheckClient(conn),
		Logger:     l,
	}
}

type checkRes struct {
	isTerrorist  bool
	isAffiliated bool
	isDoc        bool
}

func (r *checkRes) IsTerrorist() bool {
	return r.isTerrorist
}
func (r *checkRes) IsAffiliated() bool {
	return r.isAffiliated
}
func (r *checkRes) IsDocActualizationRequired() bool {
	return r.isDoc
}

func (c *CheckService) Check(ctx context.Context, p entities.Prospect) (entities.CheckResults, error) {
	userInfo := checkgrpc.UserInfo{
		Lastname:     p.FullProfile.LastName,
		Firstname:    p.FullProfile.FirstName,
		Middlename:   p.FullProfile.MiddleName,
		ExpiryDate:   timeToString(p.FullProfile.DocExpiryDate),
		BirthDate:    timeToString(p.FullProfile.BirthDate),
		IABSClientID: p.IABSClientID,
	}
	resp, err := c.grpcClient.CheckAffiliate(ctx, &userInfo)
	if err != nil {
		return nil, errs.ErrSourceConnectionErr
	}
	result := checkRes{
		isTerrorist:  resp.IsTerrorist,
		isAffiliated: resp.IsAffiliate,
		isDoc:        resp.IsExpired,
	}
	return &result, nil
}
