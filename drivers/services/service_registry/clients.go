package service_registry

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/helpers"
	"dbo/onboarding/pkg/logger"

	proto "gitlab.hamkorbank.uz/libs/protos/v2/client_search"
	"google.golang.org/grpc"
)

const (
	ErrRequestValidationCode = -(1 + iota)
	ErrNotFoundCode
	ErrFatalCode
)

type Clients struct {
	grpcClient proto.ClientSearchClient
	logger.Logger
}

func NewIABSClients(host string, l logger.Logger) *Clients {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &Clients{
		grpcClient: proto.NewClientSearchClient(conn),
		Logger:     l,
	}
}

func (c *Clients) GetByID(ctx context.Context, iabsClientID, iabsClientCode string) (entities.Profile, error) {
	resp, err := c.grpcClient.GetClientIABS(ctx, &proto.IABSRequest{IABSClientID: iabsClientID})
	if err != nil {
		return entities.Profile{}, errs.ErrSourceConnectionErr
	}
	switch resp.GetErrorCode() {
	case ErrNotFoundCode:
		return entities.Profile{}, errs.Errf(errs.ErrNotFound, resp.GetErrorMessage())
	case ErrRequestValidationCode:
		return entities.Profile{}, errs.Errf(errs.ErrValidation, resp.GetErrorMessage())
	case ErrFatalCode:
		return entities.Profile{}, errs.Errf(errs.ErrSourceConnectionErr, resp.GetErrorMessage())
	default:
		profile := iabsToProfile(resp.GetIABS(), iabsClientID)
		profile.IABSClientCode = iabsClientCode
		profile.IABSClientID = iabsClientID
		return profile, err
	}
}

func guessDocType(docSeries string) string {
	if helpers.StrArrIncludes([]string{"AA", "AB", "AC", "KA"}, docSeries) {
		return "6"
	}
	if helpers.StrArrIncludes([]string{"AD"}, docSeries) {
		return "0"
	}
	return ""
}

func (c *Clients) Search(ctx context.Context, data entities.IdentityData) (entities.Profile, error) {
	var bd, docType string
	if data.BirthDate != nil {
		bd = data.BirthDate.Format("")
	}
	if data.DocSeries != "" {
		docType = guessDocType(data.DocSeries)
	}
	if data.IABSClientID != "" {
		return c.GetByID(ctx, data.IABSClientID, "")
	}
	resp, err := c.grpcClient.SearchClient(ctx, &proto.SearchRequest{
		IABSClientCode: data.IABSClientID,
		Series:         data.DocSeries,
		Number:         data.DocNumber,
		DocTypeIabs:    docType,
		BirthDate:      bd,
		INPS:           data.PINFL,
	})
	if err != nil {
		return entities.Profile{}, errs.ErrSourceConnectionErr
	}
	switch resp.GetErrorCode() {
	case ErrNotFoundCode:
		return entities.Profile{}, errs.Errf(errs.ErrNotFound, resp.GetErrorMessage())
	case ErrRequestValidationCode:
		return entities.Profile{}, errs.Errf(errs.ErrValidation, resp.GetErrorMessage())
	case ErrFatalCode:
		return entities.Profile{}, errs.Errf(errs.ErrSourceConnectionErr, resp.GetErrorMessage())
	default:
		return c.GetByID(ctx, resp.GetData().IABSClientID, resp.GetData().IABSClientCode)
	}
}

func iabsToProfile(iabs *proto.IABS, iabsClientID string) entities.Profile {
	p := entities.Profile{
		IABSClientID:        iabsClientID,
		IABSClientCode:      iabs.GetClientCode(),
		Source:              entities.ProfileSourceIABS,
		FirstName:           iabs.GetFirstname(),
		LastName:            iabs.GetLastname(),
		MiddleName:          iabs.GetMiddlename(),
		FirstNameEn:         "",
		LastNameEn:          "",
		BirthPlace:          iabs.GetBirthPlace(),
		BirthDate:           pbTimeToTime(iabs.BirthDate),
		BirthCountryCode:    iabs.GetBirthPlaceCountry(),
		Gender:              iabs.GetGender(),
		TIN:                 iabs.GetTIN(),
		PINFL:               iabs.GetPINFL(),
		INPS:                iabs.GetINPS(),
		NationalityCode:     iabs.GetLocalityName(),
		CitizenshipCode:     "", // Map Country to
		TINRegistrationDate: nil,
		TINRegistrationGNI:  "",
		Document: entities.Document{
			DocType:         iabs.GetDocType(),
			DocSeries:       iabs.GetDocSeries(),
			DocNumber:       iabs.GetDocNumber(),
			DocExpiryDate:   pbTimeToTime(iabs.GetDocExpiryDate()),
			DocIssueDate:    pbTimeToTime(iabs.GetDocIssueDate()),
			DocIssueOrgCode: "",
			DocIssueOrgDesc: iabs.GetDocIssuer(),
			DocRegionCode:   iabs.GetDocRegionCode(),
			DocDistrictCode: "",
		},
	}
	addr := []entities.Address{{
		Type:         entities.AddressTypeDomicile,
		Address:      iabs.GetAddress(),
		CountryCode:  iabs.GetCountryCode(),
		RegionCode:   iabs.GetRegionCode(),
		DistrictCode: iabs.GetDistrictCode(),
		PlaceDesc:    iabs.GetLocalityName(),
	}}
	p.SetAddresses(addr)
	return p
}
