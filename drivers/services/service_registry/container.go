package service_registry

import (
	"dbo/onboarding/config"
	"dbo/onboarding/contracts"
	"dbo/onboarding/drivers/services"
	"dbo/onboarding/pkg/logger"
)

type Container struct {
	clients          *Clients
	nibbd            *NIBBD
	security         *Security
	check            *CheckService
	references       *References
	productURLSource *ProductsCatalog
	signature        *Signature
}

func (c *Container) Check() contracts.IChecker {
	return c.check
}
func (c *Container) Security() services.Security {
	return c.security
}

func (c *Container) Signature() contracts.ISignatureService {
	return c.signature
}

func (c *Container) References() contracts.IReferences {
	return c.references
}

func (c *Container) Offers() contracts.IMasterOfferSource {
	return c.references
}

func (c *Container) Clients() contracts.IProfileSearcher {
	return c.clients
}

func (c *Container) NIBBD() contracts.IProfileSearcher {
	return c.nibbd
}
func (c *Container) ProductURL() contracts.IProductCatalog {
	return c.productURLSource
}

func New(c config.Config, l logger.Logger) Container {
	return Container{
		clients:          NewIABSClients(c.ClientSearchGRPC, l),
		nibbd:            NewNIBBD(c.NibbdGRPC, l),
		security:         newSecurity(c.SecurityGRPC, l),
		check:            newCheck(c.CheckGRPC, l),
		references:       newReferences(c.ReferencesGRPC, l),
		productURLSource: NewProductCatalog(c.ProductCatalogGRPC, l),
		signature:        NewSignature(l, c.SignatureURL),
	}
}
