package service_registry

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"

	"gitlab.hamkorbank.uz/libs/remote"
)

type Signature struct {
	logger.Logger
	httpClient *remote.Service
}

func NewSignature(l logger.Logger, baseURL string) *Signature {
	httpClient := remote.New(baseURL, l)
	return &Signature{
		Logger:     l,
		httpClient: httpClient,
	}
}

func (s *Signature) InitSign(ctx context.Context, contact entities.Contact) (string, error) {
	dboResp := remote.DBOResponse{}
	err := s.httpClient.Post("/signature/simple", initSignReq{
		Phone: contact.Value,
		TTL:   90,
	}, &dboResp)
	if err != nil {
		return "", errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	resp := initSignResp{}
	if err := dboResp.Scan(&resp); err != nil {
		return "", errs.Errf(errs.ErrSourceInternal, "Error: %v", err.Error())
	}
	return resp.SignID, nil
}

type initSignReq struct {
	Phone string `json:"phone"`
	TTL   int    `json:"ttl"`
}
type initSignResp struct {
	SignID string `json:"sign_id"`
}

func (s *Signature) Sign(ctx context.Context, code, signatureID string) error {
	dboResp := remote.DBOResponse{}
	err := s.httpClient.Post("/signature/sign/simple", signReq{
		SignID: signatureID,
		Code:   code,
	}, &dboResp)
	if err != nil {
		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if err := dboResp.ToError(); err != nil {
		return errs.Errf(errs.ErrSourceInternal, "%v %v", err.Error(), dboResp.ErrorNote)
	}
	return nil
}

type signReq struct {
	SignID string `json:"sign_id"`
	Code   string `json:"code"`
}
