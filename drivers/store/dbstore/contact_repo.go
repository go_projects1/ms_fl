package dbstore

import (
	"context"
	"database/sql"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"errors"
	"time"
)

type contactsRepo struct {
	logger.Logger
	store *DBStore
}

// FindByID
// Errors: entities.ErrNotFound, entities.ErrSourceConnectionErr
func (repo *contactsRepo) FindByID(ctx context.Context, id string) (entities.Contact, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	record := dbcontact{}
	q := `SELECT * FROM contacts WHERE id = $1`
	if err := sqlClient.Get(&record, q, id); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return entities.Contact{}, errs.Errf(errs.ErrNotFound, "Contact not found. id %v", id)
		}
		return entities.Contact{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}
	return record.toModel(), nil
}

// Store
// Errors: entities.ErrWrongInput, entities.ErrSourceInternal
func (repo *contactsRepo) Store(ctx context.Context, c entities.Contact) (entities.Contact, error) {
	if c.ID != "" {
		return c, errs.Errf(errs.ErrWrongInput, "[contacts.Store]: Contact ID is present")
	}
	if c.ProspectID == "" {
		return c, errs.Errf(errs.ErrWrongInput, "[contacts.Store]: Prospect ID is missing")
	}
	sqlClient := repo.store.sqlClientByCtx(ctx)
	c.CreatedAt = time.Now()
	q := `INSERT INTO contacts (prospect_id,value,type,verified_at,created_at) VALUES ($1,$2,$3,$4,$5) RETURNING ID `

	if err := sqlClient.QueryRowx(q, c.ProspectID, c.Value, c.Type, c.VerifiedAt, c.CreatedAt).Scan(&c.ID); err != nil {
		return c, errs.Errf(errs.ErrSourceInternal, "contact store: %v", err.Error())
	}
	return c, nil
}

// GetByProspect
// Errors: entities.ErrSourceInternal
func (repo *contactsRepo) GetByProspect(ctx context.Context, prospectID string) ([]entities.Contact, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT * FROM contacts WHERE prospect_id = $1`
	var records []dbcontact

	if err := sqlClient.Select(&records, q, prospectID); err != nil {
		return nil, errs.Errf(errs.ErrSourceInternal, "GetByProspect: %v", err.Error())
	}
	result := make([]entities.Contact, len(records))
	for i, raw := range records {
		result[i] = raw.toModel()
	}
	return result, nil
}

// Find
// Errors: entities.ErrSourceInternal
func (repo *contactsRepo) Find(ctx context.Context, value string) ([]entities.Contact, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT * FROM contacts WHERE value = $1`
	records := []dbcontact{}

	if err := sqlClient.Select(&records, q, value); err != nil {
		repo.Error(err.Error())
		return nil, errs.Errf(errs.ErrSourceInternal, "contact find: %v", err.Error())
	}
	result := make([]entities.Contact, len(records))
	for i, raw := range records {
		result[i] = raw.toModel()
	}
	return result, nil
}

// Update
// Errors: entities.ErrSourceInternal,entities.ErrNotFound
func (repo *contactsRepo) Update(ctx context.Context, c entities.Contact) (entities.Contact, error) {
	if c.ID == "" || c.ProspectID == "" {
		return c, errs.Errf(errs.ErrWrongInput, "updated validation failed - (id: %v, prospect_id:%v)", c.ID, c.ProspectID)
	}
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `UPDATE contacts SET prospect_id=$1,value=$2,type=$3,verified_at=$4,created_at=$5 WHERE id = $6 `
	result, err := sqlClient.Exec(q, c.ProspectID, c.Type, c.Value, c.VerifiedAt, c.CreatedAt, c.ID)
	if err != nil {
		return c, errs.Errf(errs.ErrSourceInternal, "contact update: %v", err.Error())
	}
	if count, _ := result.RowsAffected(); count == 0 {
		return c, errs.Errf(errs.ErrNotFound, "contact not found: %v", c.ID)
	}
	return c, nil
}

// Delete
// Errors: entities.ErrSourceInternal, entities.ErrNotFound
func (repo *contactsRepo) Delete(ctx context.Context, c entities.Contact) error {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `DELETE FROM contacts WHERE id=$1`
	res, err := sqlClient.Exec(q, c.ID)
	if err != nil {
		return errs.Errf(errs.ErrSourceInternal, "contact delete: %v", err.Error())
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return errs.Errf(errs.ErrNotFound, "Contact Not Found: %v", c.ID)
	}
	return nil
}
