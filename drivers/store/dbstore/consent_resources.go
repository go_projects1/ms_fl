package dbstore

import (
	"database/sql"
	"dbo/onboarding/entities"
	"time"
)

type dbconsent struct {
	ID               string       `db:"id"`
	ProspectID       string       `db:"prospect_id"`
	SignID           string       `db:"sign_id"`
	OfferCode        string       `db:"offer_code"`
	OfferVersionCode string       `db:"offer_version_code"`
	AcceptedAt       sql.NullTime `db:"accepted_at"`
	CreatedAt        time.Time    `db:"created_at"`
}

func dbconsentFromModel(m entities.Consent) dbconsent {
	record := dbconsent{
		ID:               m.ID,
		ProspectID:       m.ProspectID,
		SignID:           m.SignID,
		OfferCode:        m.OfferCode,
		OfferVersionCode: m.OfferVersionCode,
		CreatedAt:        m.CreatedAt,
	}
	if m.AcceptedAt != nil {
		record.AcceptedAt = sql.NullTime{
			Time:  *m.AcceptedAt,
			Valid: true,
		}
	}
	return record
}

func (raw dbconsent) toModel() entities.Consent {
	consent := entities.Consent{
		ID:               raw.ID,
		ProspectID:       raw.ProspectID,
		SignID:           raw.SignID,
		OfferCode:        raw.OfferCode,
		OfferVersionCode: raw.OfferVersionCode,
		CreatedAt:        raw.CreatedAt,
	}
	if raw.AcceptedAt.Valid {
		consent.AcceptedAt = &raw.AcceptedAt.Time
	}
	return consent
}
