package dbstore

import (
	"context"
	"database/sql"
	"dbo/onboarding/entities"
	errs "dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"errors"
	"time"
)

type prospectRepo struct {
	logger.Logger
	store *DBStore
}

// Get
// Errors: entities.ErrSourceInternal, entities.ErrNotFound
func (repo *prospectRepo) Get(ctx context.Context, id string) (entities.Prospect, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	record := dbprospect{}
	q := `SELECT * FROM prospects WHERE id=$1`
	if err := sqlClient.Get(&record, q, id); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return entities.Prospect{}, errs.Errf(errs.ErrNotFound, err.Error())
		}
		return entities.Prospect{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return record.toModel(), nil
}

// GetByContact
// Errors: entities.ErrSourceInternal, entities.ErrNotFound
func (repo *prospectRepo) GetByContact(ctx context.Context, c entities.Contact) (entities.Prospect, error) {
	var prospect entities.Prospect
	sqlClient := repo.store.sqlClientByCtx(ctx)
	if !c.IsVerified() {
		return prospect, errs.Errf(errs.ErrWrongInput, "Contact: {%v:%v} is not verified", c.Value, c.Type)
	}
	q := `SELECT t1.* FROM prospects t1 INNER JOIN contacts t2 ON t1.ID = t2.prospect_id 
			WHERE t2.value=$1 AND t2.type=$2 AND t2.verified_at IS NOT NULL`
	record := dbprospect{}
	if err := sqlClient.Get(&record, q, c.Value, c.Type); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return prospect, errs.Errf(errs.ErrNotFound, err.Error())
		}
		return prospect, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}
	return record.toModel(), nil
}

func (repo *prospectRepo) Find(ctx context.Context, data entities.IdentityData) (entities.Prospect, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	var p entities.Prospect
	record := dbprospect{}
	if data.IABSClientID != "" {
		q := "SELECT * FROM prospects WHERE iabs_client_id=$1"
		if err := sqlClient.Get(&record, q, data.IABSClientID); err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				data.IABSClientID = ""
				repo.store.log.Debug("prospectRepo.Find  not found by IABSClientID")
				return repo.Find(ctx, data)
			}
			return p, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
		}
		return record.toModel(), nil
	}
	if data.PINFL != "" {
		q := "SELECT t1.* FROM prospects t1 WHERE t1.pinfl = $1"
		if err := sqlClient.Get(&record, q, data.PINFL); err != nil && !errors.Is(err, sql.ErrNoRows) {
			if errors.Is(err, sql.ErrNoRows) {
				data.PINFL = ""
				repo.store.log.Debug("prospectRepo.Find  not found by PINFL")
				return repo.Find(ctx, data)
			}
			return p, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
		}
		return record.toModel(), nil
	}
	if data.BirthDate != nil && data.DocSeries != "" && data.DocNumber != "" {
		q := `SELECT t1.* FROM prospects t1 
				WHERE t1.birth_date = $1 AND t2.doc_series = $2 AND t1.doc_number = $3`

		if err := sqlClient.Get(&record, q, data.BirthDate, data.DocSeries, data.DocNumber); err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				repo.store.log.Debug("prospectRepo.Find  not found by DocSeries && DocNumber")
				return p, errs.Errf(errs.ErrNotFound, err.Error())
			}
			return p, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
		}
		return record.toModel(), nil
	}
	return p, errs.Errf(errs.ErrNotFound, "%+v", data)
}

func (repo *prospectRepo) Store(ctx context.Context, p entities.Prospect) (entities.Prospect, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	if p.ID != "" {
		return p, errs.Errf(errs.ErrWrongInput, "prospect store:prospect id is present: %v;", p.ID)
	}
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
	raw := dbprospectFromModel(p)
	q := `INSERT INTO prospects
    		(
    		 state,iabs_client_id,rs_client_id,created_at,updated_at,
    		 verified_at,checked_at,
    		 	is_affiliated,is_terrorist,is_profile_complete,
				approved_at,pinfl,birth_date,doc_series, doc_number
    		 	) 
			VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15) RETURNING id`
	if err := sqlClient.QueryRowx(q,
		raw.State, p.IABSClientID, p.RSClientID, p.CreatedAt, p.UpdatedAt,
		raw.VerifiedAt, raw.CheckedAt,
		raw.IsAffiliated, raw.IsTerrorist, raw.IsProfileComplete,
		raw.ApprovedAt, raw.PINFL, raw.BirthDate, raw.DocSeries, raw.DocNumber,
	).Scan(&p.ID); err != nil {
		return p, errs.Errf(errs.ErrSourceConnectionErr, "prospect store:%v", err.Error())
	}
	return p, nil
}

func (repo *prospectRepo) Update(ctx context.Context, p entities.Prospect) (entities.Prospect, error) {
	if p.ID == "" {
		return p, errs.Errf(errs.ErrWrongInput, "prospect update:prospect id is absent")
	}
	p.UpdatedAt = time.Now()
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `UPDATE prospects SET state=$1,iabs_client_id=$2,rs_client_id=$3, updated_at=$4,
                     verified_at=$5,checked_at=$6,
                     is_terrorist=$7,is_affiliated=$8,is_profile_complete=$9,is_consent_needed=$10,
					 approved_at=$11,pinfl=$12, birth_date=$13, doc_series=$14, doc_number=$15,iabs_client_code=$16
                     WHERE id=$17`
	raw := dbprospectFromModel(p)
	results, err := sqlClient.Exec(q, raw.State, raw.IABSClientID, raw.RSID, raw.UpdatedAt,
		raw.VerifiedAt, raw.CheckedAt,
		raw.IsTerrorist, raw.IsAffiliated, raw.IsProfileComplete, raw.IsConsentNeeded,
		raw.ApprovedAt, raw.PINFL, raw.BirthDate, raw.DocSeries, raw.DocNumber, raw.IABSClientCode,
		p.ID)
	if err != nil {
		return p, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	rows, _ := results.RowsAffected()
	if rows != 1 {
		return p, errs.Errf(errs.ErrNotFound, "no rows affected")
	}
	return p, nil
}
