package dbstore

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"fmt"
	"time"
)

type appealRepo struct {
	logger.Logger
	store *DBStore
}

// Store
// Errors: entities.ErrSourceInternal
func (repo *appealRepo) Store(ctx context.Context, appeal entities.Appeal) (entities.Appeal, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	appeal.CreatedAt = time.Now()
	q := `INSERT INTO appeals(prospect_id,product_id,created_at) VALUES($1,$2,$3) RETURNING id  `

	if err := sqlClient.Get(&appeal.ID, q, appeal.ProspectID, appeal.ProductID, appeal.CreatedAt); err != nil {
		return appeal, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return appeal, nil
}

// GetByProspect
// Errors: entities.ErrSourceInternal
func (repo *appealRepo) GetByProspect(ctx context.Context, prospectID string) ([]entities.Appeal, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT id,prospect_id,product_id,created_at FROM appeals WHERE prospect_id=$1 ORDER BY created_at`
	raws, err := sqlClient.Queryx(q, prospectID)
	if err != nil {
		repo.Error(fmt.Sprintf("AppealRepo: GetByProspect: %v", err.Error()))
		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	result := make([]entities.Appeal, 0)
	for raws.Next() {
		appeal := entities.Appeal{}
		if err := raws.Scan(&appeal.ID, &appeal.ProspectID, &appeal.ProductID, &appeal.CreatedAt); err != nil {
			return nil, errs.Errf(errs.ErrSourceInternal, "AppealRepo: GetByProspect: %v", err.Error())
		}
		result = append(result, appeal)
	}
	return result, nil
}
