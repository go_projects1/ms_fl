package dbstore

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"fmt"
)

type addressRepo struct {
	logger.Logger
	store *DBStore
}

// Store
// Errors: entities.ErrSourceInternal
func (repo *addressRepo) Store(ctx context.Context, addr entities.Address) (entities.Address, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `INSERT INTO addresses 
		(profile_id,type,address,registered_at,
country_code, region_code,district_code,
block,flat,house,place_desc,street_desc,kadastr)
VALUES ($1,$2,$3,$4,   $5,$6,$7,    $8,$9,$10,$11,$12,$13) RETURNING id`
	r := dbaddressFromModel(addr)
	err := sqlClient.Get(
		&addr.ID, q,
		r.ProfileID, r.Type, r.Address, r.RegisteredAt,
		r.CountryCode, r.RegionCode, r.DistrictCode,
		r.Block, r.Flat, r.House, r.PlaceDesc, r.StreetDesc, r.Kadastr,
	)
	if err != nil {
		repo.Debug(fmt.Sprintf("[addressRepo] Store: %v", err.Error()))
		return addr, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return addr, nil
}

// GetByProfile
// Errors: entities.ErrSourceInternal
func (repo *addressRepo) GetByProfile(ctx context.Context, profileID string) ([]entities.Address, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT * FROM addresses WHERE profile_id=$1`
	records := make([]dbaddress, 0)
	err := sqlClient.Select(&records, q, profileID)
	if err != nil {
		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	addresses := make([]entities.Address, len(records))
	for i := 0; i < len(records); i++ {
		addresses[i] = records[i].toModel()
	}
	return addresses, nil
}

// Delete
// Errors: entities.ErrSourceInternal,entities.ErrWrongInput
func (repo *addressRepo) Delete(ctx context.Context, address entities.Address) error {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	if address.ID == "" {
		return errs.Errf(errs.ErrWrongInput, "Empty address ID")
	}
	q := `DELETE FROM addresses WHERE id=$1`
	if _, err := sqlClient.Exec(q, address.ID); err != nil {
		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return nil
}

func (repo *addressRepo) Update(ctx context.Context, addr entities.Address) (entities.Address, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `UPDATE addresses SET
		(profile_id=$1,type=$2,address=$3,registered_at=$4,
		country_code=$5, region_code=$6,district_code=$7,
		block=$8,flat=$9,house=$10,place_desc=$11,street_desc=$12,kadastr=$13)
		WHERE id=$14`
	r := dbaddressFromModel(addr)
	result, err := sqlClient.Exec(
		q,
		r.ProfileID, r.Type, r.Address, r.RegisteredAt,
		r.CountryCode, r.RegionCode, r.DistrictCode,
		r.Block, r.Flat, r.House, r.PlaceDesc, r.StreetDesc, r.Kadastr,
		addr.ID,
	)
	if err != nil {
		repo.Debug(fmt.Sprintf("[addressRepo] Update: %v", err.Error()))
		return addr, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if count, _ := result.RowsAffected(); count != 1 {
		return addr, errs.Errf(errs.ErrNotFound, "no address record updated")
	}
	return addr, nil
}
