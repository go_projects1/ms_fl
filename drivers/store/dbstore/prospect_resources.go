package dbstore

import (
	"database/sql"
	"dbo/onboarding/entities"
	"time"
)

type dbprospect struct {
	ID                string         `db:"id"`
	IABSClientID      sql.NullString `db:"iabs_client_id"`
	IABSClientCode    sql.NullString `db:"iabs_client_code"`
	RSID              sql.NullString `db:"rs_client_id"`
	State             int8           `db:"state"`
	VerifiedAt        sql.NullTime   `db:"verified_at"`
	CheckedAt         sql.NullTime   `db:"checked_at"`
	CreatedAt         time.Time      `db:"created_at"`
	UpdatedAt         time.Time      `db:"updated_at"`
	IsAffiliated      bool           `db:"is_affiliated"`
	IsConsentNeeded   bool           `db:"is_consent_needed"`
	IsTerrorist       bool           `db:"is_terrorist"`
	IsProfileComplete bool           `db:"is_profile_complete"`

	PINFL      sql.NullString `db:"pinfl"`
	DocSeries  sql.NullString `db:"doc_series"`
	DocNumber  sql.NullString `db:"doc_number"`
	BirthDate  sql.NullTime   `db:"birth_date"`
	ApprovedAt sql.NullTime   `db:"approved_at"`
}

func (raw dbprospect) toModel() entities.Prospect {
	var verifiedAt, checkedAt, birthDate, approvedAt *time.Time
	if raw.VerifiedAt.Valid {
		verifiedAt = &raw.VerifiedAt.Time
	}
	if raw.CheckedAt.Valid {
		checkedAt = &raw.CheckedAt.Time
	}
	if raw.BirthDate.Valid {
		birthDate = &raw.BirthDate.Time
	}
	if raw.ApprovedAt.Valid {
		approvedAt = &raw.ApprovedAt.Time
	}
	return entities.Prospect{
		IdentityData: entities.IdentityData{
			PINFL:          raw.PINFL.String,
			DocSeries:      raw.DocSeries.String,
			DocNumber:      raw.DocNumber.String,
			BirthDate:      birthDate,
			IABSClientID:   raw.IABSClientID.String,
			IABSClientCode: raw.IABSClientCode.String,
		},
		ID:                raw.ID,
		State:             raw.State,
		RSClientID:        raw.RSID.String,
		IsAffiliated:      raw.IsAffiliated,
		IsTerrorist:       raw.IsTerrorist,
		IsProfileComplete: raw.IsProfileComplete,
		IsConsentNeeded:   raw.IsConsentNeeded,
		VerifiedAt:        verifiedAt,
		CheckedAt:         checkedAt,
		CreatedAt:         raw.CreatedAt,
		UpdatedAt:         raw.UpdatedAt,
		ApprovedAt:        approvedAt,
		Contacts:          nil,
		Profiles:          nil,
		FullProfile:       nil,
	}
}

func dbprospectFromModel(m entities.Prospect) dbprospect {
	var verifiedAt, checkedAt, birthDate, approvedAt sql.NullTime
	if m.VerifiedAt != nil {
		verifiedAt = sql.NullTime{
			Time:  *m.VerifiedAt,
			Valid: true,
		}
	}
	if m.CheckedAt != nil {
		checkedAt = sql.NullTime{
			Time:  *m.CheckedAt,
			Valid: true,
		}
	}
	if m.BirthDate != nil {
		birthDate = sql.NullTime{
			Time:  *m.BirthDate,
			Valid: true,
		}
	}
	if m.ApprovedAt != nil {
		approvedAt = sql.NullTime{
			Time:  *m.BirthDate,
			Valid: true,
		}
	}
	return dbprospect{
		ID: m.ID,
		IABSClientID: sql.NullString{
			String: m.IABSClientID,
			Valid:  m.IABSClientID != "",
		},
		IABSClientCode: sql.NullString{
			String: m.IABSClientCode,
			Valid:  m.IABSClientCode != "",
		},
		RSID: sql.NullString{
			String: m.RSClientID,
			Valid:  m.RSClientID != "",
		},
		State:             m.State,
		VerifiedAt:        verifiedAt,
		CheckedAt:         checkedAt,
		CreatedAt:         m.CreatedAt,
		UpdatedAt:         m.UpdatedAt,
		IsAffiliated:      m.IsAffiliated,
		IsTerrorist:       m.IsTerrorist,
		IsProfileComplete: m.IsProfileComplete,
		PINFL: sql.NullString{
			Valid:  m.PINFL != "",
			String: m.PINFL,
		},
		DocSeries: sql.NullString{
			Valid:  m.DocSeries != "",
			String: m.DocSeries,
		},
		DocNumber: sql.NullString{
			String: m.DocNumber,
			Valid:  m.DocNumber != "",
		},
		BirthDate:  birthDate,
		ApprovedAt: approvedAt,
	}
}
