package dbstore

import (
	"database/sql"
	"dbo/onboarding/entities"
	"time"
)

type dbprofile struct {
	ID         string `db:"id"`
	Source     int8   `db:"source"`
	ProspectID string `db:"prospect_id"`

	FirstName        string         `db:"first_name"`
	LastName         string         `db:"last_name"`
	MiddleName       string         `db:"middle_name"`
	FirstNameEn      string         `db:"first_name_en"`
	LastNameEn       string         `db:"last_name_en"`
	BirthPlace       string         `db:"birth_place"`
	BirthDate        sql.NullTime   `db:"birth_date"`
	BirthCountryCode sql.NullString `db:"birth_country_code"`
	Gender           sql.NullString `db:"gender"`

	TIN                 sql.NullString `db:"tin"`
	PINFL               sql.NullString `db:"pinfl"`
	INPS                sql.NullString `db:"inps"`
	NationalityCode     sql.NullString `db:"nationality_code"`
	CitizenshipCode     sql.NullString `db:"citizenship_code"`
	TINRegistrationDate sql.NullTime   `db:"tin_registration_date"`
	TINRegistrationGni  string         `db:"tin_registration_gni"`
	SubjectState        string         `db:"subject_state"`
	CreatedAt           time.Time      `db:"created_at"`
	UpdatedAt           time.Time      `db:"updated_at"`

	DocType         sql.NullString `db:"doc_type"`
	DocSeries       string         `db:"doc_series"`
	DocNumber       string         `db:"doc_number"`
	DocExpiryDate   sql.NullTime   `db:"doc_expiry_date"`
	DocIssueDate    sql.NullTime   `db:"doc_issue_date"`
	DocIssueOrgCode string         `db:"doc_issue_org_code"`
	DocIssueOrgDesc string         `db:"doc_issue_org_desc"`
	DocRegionCode   sql.NullString `db:"doc_region_code"`
	DocDistrictCode sql.NullString `db:"doc_district_code"`
}

func (raw dbprofile) toModel() entities.Profile {
	doc := entities.Document{
		DocSeries:       raw.DocSeries,
		DocNumber:       raw.DocNumber,
		DocIssueOrgCode: raw.DocIssueOrgCode,
		DocIssueOrgDesc: raw.DocIssueOrgDesc,
		DocRegionCode:   raw.DocRegionCode.String,
		DocDistrictCode: raw.DocDistrictCode.String,
	}
	if raw.DocExpiryDate.Valid {
		doc.DocExpiryDate = &raw.DocExpiryDate.Time
	}
	if raw.DocIssueDate.Valid {
		doc.DocExpiryDate = &raw.DocIssueDate.Time
	}
	p := entities.Profile{
		ID:                 raw.ID,
		Source:             raw.Source,
		ProspectID:         raw.ProspectID,
		FirstName:          raw.FirstName,
		LastName:           raw.LastName,
		MiddleName:         raw.MiddleName,
		FirstNameEn:        raw.FirstNameEn,
		LastNameEn:         raw.LastNameEn,
		BirthPlace:         raw.BirthPlace,
		BirthCountryCode:   raw.BirthCountryCode.String,
		Gender:             raw.Gender.String,
		TIN:                raw.TIN.String,
		PINFL:              raw.PINFL.String,
		INPS:               raw.INPS.String,
		NationalityCode:    raw.NationalityCode.String,
		CitizenshipCode:    raw.CitizenshipCode.String,
		TINRegistrationGNI: raw.TINRegistrationGni,
		SubjectState:       raw.SubjectState,
		CreatedAt:          raw.CreatedAt,
		UpdatedAt:          raw.UpdatedAt,
		Document:           doc,
	}
	if raw.BirthDate.Valid {
		p.BirthDate = &raw.BirthDate.Time
	}
	if raw.TINRegistrationDate.Valid {
		p.TINRegistrationDate = &raw.TINRegistrationDate.Time
	}
	return p
}

func (raw dbprofile) dbparams() []interface{} {
	return []interface{}{raw.Source, raw.ProspectID,
		raw.FirstName, raw.LastName, raw.MiddleName, raw.FirstNameEn, raw.LastNameEn,
		raw.BirthPlace, raw.BirthDate, raw.BirthCountryCode, raw.Gender,
		raw.TIN, raw.PINFL, raw.INPS,
		raw.NationalityCode, raw.CitizenshipCode, raw.TINRegistrationDate, raw.TINRegistrationGni,
		raw.SubjectState,
		raw.DocType, raw.DocSeries, raw.DocNumber, raw.DocExpiryDate, raw.DocIssueDate,
		raw.DocIssueOrgCode, raw.DocIssueOrgDesc, raw.DocRegionCode, raw.DocDistrictCode,
		raw.CreatedAt, raw.UpdatedAt,
	}
}

func dbprofileFromModel(profile entities.Profile) dbprofile {
	raw := dbprofile{
		ID:          profile.ID,
		Source:      profile.Source,
		ProspectID:  profile.ProspectID,
		FirstName:   profile.FirstName,
		LastName:    profile.LastName,
		MiddleName:  profile.MiddleName,
		FirstNameEn: profile.FirstNameEn,
		LastNameEn:  profile.LastNameEn,
		BirthPlace:  profile.BirthPlace,
		BirthCountryCode: sql.NullString{
			String: profile.BirthCountryCode,
			Valid:  len(profile.BirthCountryCode) == 3,
		},
		Gender: sql.NullString{
			String: profile.Gender,
			Valid:  len(profile.Gender) == 1,
		},
		TIN: sql.NullString{
			String: profile.TIN,
			Valid:  len(profile.TIN) == 9,
		},
		PINFL: sql.NullString{
			String: profile.PINFL,
			Valid:  len(profile.PINFL) == 14,
		},
		INPS: sql.NullString{
			String: profile.INPS,
			Valid:  len(profile.INPS) == 14,
		},
		NationalityCode: sql.NullString{
			String: profile.NationalityCode,
			Valid:  len(profile.NationalityCode) == 3,
		},
		CitizenshipCode: sql.NullString{
			String: profile.CitizenshipCode,
			Valid:  len(profile.CitizenshipCode) == 3,
		},
		TINRegistrationGni: profile.TINRegistrationGNI,
		SubjectState:       profile.SubjectState,
		DocType: sql.NullString{
			String: profile.DocType,
			Valid:  len(profile.DocType) == 1,
		},
		DocSeries:       profile.DocSeries,
		DocNumber:       profile.DocNumber,
		DocIssueOrgCode: profile.DocIssueOrgCode,
		DocIssueOrgDesc: profile.DocIssueOrgDesc,
		DocRegionCode: sql.NullString{
			String: profile.DocRegionCode,
			Valid:  len(profile.DocRegionCode) == 2,
		}, DocDistrictCode: sql.NullString{
			String: profile.DocDistrictCode,
			Valid:  len(profile.DocDistrictCode) == 3,
		},
		CreatedAt: profile.CreatedAt,
		UpdatedAt: profile.UpdatedAt,
	}
	if profile.BirthDate != nil {
		raw.BirthDate = sql.NullTime{
			Time:  *profile.BirthDate,
			Valid: true,
		}
	}
	if profile.TINRegistrationDate != nil {
		raw.TINRegistrationDate = sql.NullTime{
			Time:  *profile.TINRegistrationDate,
			Valid: true,
		}
	}
	if profile.DocExpiryDate != nil {
		raw.DocExpiryDate = sql.NullTime{
			Time:  *profile.DocExpiryDate,
			Valid: true,
		}
	}
	if profile.DocIssueDate != nil {
		raw.DocIssueDate = sql.NullTime{
			Time:  *profile.DocIssueDate,
			Valid: true,
		}
	}
	return raw
}
