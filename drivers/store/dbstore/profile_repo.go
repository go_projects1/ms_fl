package dbstore

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"fmt"
	"time"
)

type profileRepo struct {
	logger.Logger
	store *DBStore
}

// Delete
// Errors: entities.ErrSourceInternal, entities.ErrNotFound
func (repo *profileRepo) Delete(ctx context.Context, profile entities.Profile) error {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `DELETE FROM profiles WHERE id=$1`
	res, err := sqlClient.Exec(q, profile.ID)
	if err != nil {
		repo.Debug(err.Error())
		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if count, _ := res.RowsAffected(); count != 1 {
		repo.Debug(fmt.Sprintf("rows affected:%v", count))
		return errs.Errf(errs.ErrNotFound, "rows affected:%v", count)
	}
	return nil
}

// GetByProspectInTx
// Errors: entities.ErrSourceInternal
func (repo *profileRepo) GetByProspect(ctx context.Context, prospectID string) ([]entities.Profile, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `select * from profiles where prospect_id = $1`
	var records []dbprofile
	if err := sqlClient.Select(&records, q, prospectID); err != nil {
		return nil, errs.Errf(errs.ErrSourceInternal, "profile get_by_prospect: %v", err.Error())
	}

	result := make([]entities.Profile, len(records))
	for i := range records {
		result[i] = records[i].toModel()
	}
	return result, nil
}

// Store
// Errors: entities.ErrSourceInternal
func (repo *profileRepo) Store(ctx context.Context, profile entities.Profile) (entities.Profile, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	profile.CreatedAt = time.Now()
	profile.UpdatedAt = time.Now()
	q := `INSERT INTO profiles (
	source,prospect_id,
	first_name,last_name,middle_name,first_name_en,last_name_en,
	birth_place,birth_date,birth_country_code,gender, 
	tin,pinfl,inps,
	nationality_code,citizenship_code,tin_registration_date,tin_registration_gni,subject_state,
	doc_type,doc_series,doc_number,doc_expiry_date,doc_issue_date,doc_issue_org_code,doc_issue_org_desc,doc_region_code,doc_district_code,
	created_at,updated_at
 ) VALUES(
	$1,$2,
	$3,$4,$5,$6,$7,
	$8,$9,$10,$11,
	$12,$13,$14,
	$15,$16,$17,$18,$19,
	$20,$21,$22,$23,$24,$25,$26,$27,
	$28,$29,$30
 ) RETURNING id`
	raw := dbprofileFromModel(profile)
	params := raw.dbparams()
	if err := sqlClient.Get(&profile.ID, q, params...); err != nil {
		return profile, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return profile, nil
}

// Update
// Errors: entities.ErrSourceInternal, entities.ErrNotFound
func (repo *profileRepo) Update(ctx context.Context, profile entities.Profile) (entities.Profile, error) {
	if profile.ID == "" {
		return profile, errs.Errf(errs.ErrWrongInput, "No profile ID: %v", profile.ID)
	}
	sqlClient := repo.store.sqlClientByCtx(ctx)
	profile.UpdatedAt = time.Now()
	q := `UPDATE profiles SET source=$1, prospect_id=$2,
			first_name=$3,last_name=$4,middle_name=$5,first_name_en=$6,last_name_en=$7,
			birth_place=$8,birth_date=$9,birth_country_code=$10,gender=$11,
			tin=$12,pinfl=$13,inps=$14,
			nationality_code=$15,citizenship_code=$16,tin_registration_date=$17, tin_registration_gni=$18,subject_state=$19,
			doc_series=$20,doc_type=$21,doc_number=$22,doc_expiry_date=$23, doc_issue_date=$24, doc_issue_org_code=$25,
			doc_issue_org_desc=$26, doc_region_code=$27, doc_district_code=$28,
			created_at=$29,updated_at=$30
		WHERE id=$31
		`
	raw := dbprofileFromModel(profile)
	params := raw.dbparams()
	params = append(params, raw.ID)
	res, err := sqlClient.Exec(q, params...)
	if err != nil {
		repo.Debug(err.Error())
		return profile, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if count, _ := res.RowsAffected(); count != 1 {
		repo.Debug(fmt.Sprintf("rows affected:%v", count))
		return profile, errs.Errf(errs.ErrNotFound, "rows affected:%v", count)
	}
	return profile, nil
}
