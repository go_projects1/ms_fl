package dbstore

import (
	"database/sql"
	"dbo/onboarding/entities"
)

type dbaddress struct {
	ID           string       `db:"id"`
	ProfileID    string       `db:"profile_id"`
	Type         int8         `db:"type"`
	Address      string       `db:"address"`
	RegisteredAt sql.NullTime `db:"registered_at"`

	CountryCode  sql.NullString `db:"country_code"`
	RegionCode   sql.NullString `db:"region_code"`
	DistrictCode sql.NullString `db:"district_code"`

	Block      string `db:"block"`
	Flat       string `db:"flat"`
	House      string `db:"house"`
	PlaceDesc  string `db:"place_desc"`
	StreetDesc string `db:"street_desc"`
	Kadastr    string `db:"kadastr"`
}

func dbaddressFromModel(m entities.Address) dbaddress {
	record := dbaddress{
		ID:        m.ID,
		ProfileID: m.ProfileID,
		Type:      m.Type,
		Address:   m.Address,
		CountryCode: sql.NullString{
			Valid:  len(m.CountryCode) == 3,
			String: m.CountryCode,
		},
		RegionCode: sql.NullString{
			Valid:  len(m.RegionCode) == 2,
			String: m.RegionCode,
		},
		DistrictCode: sql.NullString{
			Valid:  len(m.DistrictCode) == 3,
			String: m.DistrictCode,
		},
		Block:      m.Block,
		Flat:       m.Flat,
		House:      m.House,
		PlaceDesc:  m.PlaceDesc,
		StreetDesc: m.StreetDesc,
		Kadastr:    m.Kadastr,
	}
	if m.RegisteredAt != nil {
		record.RegisteredAt = sql.NullTime{
			Time:  *m.RegisteredAt,
			Valid: true,
		}
	}
	return record
}

func (raw dbaddress) toModel() entities.Address {
	consent := entities.Address{
		ID:        raw.ID,
		ProfileID: raw.ProfileID,
		Type:      raw.Type,
		Address:   raw.Address,

		CountryCode:  raw.CountryCode.String,
		RegionCode:   raw.RegionCode.String,
		DistrictCode: raw.DistrictCode.String,
		Block:        raw.Block,
		Flat:         raw.Flat,
		House:        raw.House,
		PlaceDesc:    raw.PlaceDesc,
		StreetDesc:   raw.StreetDesc,
		Kadastr:      raw.Kadastr,
	}
	if raw.RegisteredAt.Valid {
		consent.RegisteredAt = &raw.RegisteredAt.Time
	}
	return consent
}
