package dbstore

import (
	"context"
	"database/sql"
	"dbo/onboarding/contracts"
	"dbo/onboarding/drivers/store"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"

	"github.com/jmoiron/sqlx"
	// "gitlab.hamkorbank.uz/libs/log"
)

type contextKey string

var TxContextKey contextKey = "TX_CONTEXT_KEY"

type DBStore struct {
	log       logger.Logger
	db        *sqlx.DB
	appeals   *appealRepo
	addresses *addressRepo
	prospects *prospectRepo
	profiles  *profileRepo
	contacts  *contactsRepo
	consents  *consentRepo
}

func New(log logger.Logger, db *sqlx.DB) store.Store {
	return &DBStore{
		log:       log,
		db:        db,
		appeals:   nil,
		prospects: nil,
		profiles:  nil,
		contacts:  nil,
		consents:  nil,
	}
}

func (s *DBStore) Addresses() store.AddressRepo {
	if s.addresses == nil {
		s.addresses = &addressRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.addresses
}
func (s *DBStore) Prospects() store.ProspectRepo {
	if s.prospects == nil {
		s.prospects = &prospectRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.prospects
}

func (s *DBStore) Contacts() store.ContactRepo {
	if s.contacts == nil {
		s.contacts = &contactsRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.contacts
}

func (s *DBStore) Profiles() store.ProfileRepo {
	if s.profiles == nil {
		s.profiles = &profileRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.profiles
}

func (s *DBStore) Consents() store.ConsentRepo {
	if s.consents == nil {
		s.consents = &consentRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.consents
}

func (s *DBStore) Appeals() store.AppealRepo {
	if s.appeals == nil {
		s.appeals = &appealRepo{
			Logger: s.log,
			store:  s,
		}
	}
	return s.appeals
}

type dbSession struct {
	tx *sqlx.Tx
	logger.Logger
	ctx context.Context
}

func (s *dbSession) Close(err error) {
	if err != nil {
		s.Debug(err.Error())
		s.Debug("Error inside transaction: Rolling back...")
		_ = s.tx.Rollback()
		return
	}
	if err := s.tx.Commit(); err != nil {
		s.Error("Commit Error")
	} else {
		s.Debug("Commit successfully")
	}
}

func (s *DBStore) StartSession(c context.Context) (contracts.Session, context.Context, error) {
	tx, err := s.db.Beginx()
	if err != nil {
		return nil, nil, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}
	c = context.WithValue(c, TxContextKey, tx)
	return &dbSession{tx, s.log, c}, c, nil
}

func (s *DBStore) sqlClientByCtx(ctx context.Context) sqlClient {
	if ctx == nil {
		return s.db
	}
	val := ctx.Value(TxContextKey)
	if val == nil {
		return s.db
	}
	tx, ok := val.(*sqlx.Tx)
	if !ok {
		return s.db
	}
	return tx
}

// sqlClient - common interface for *sqlx.DB and *sqlx.TX
// https://gist.github.com/hielfx/4469d35127d085fc3501d483e34d4bad
type sqlClient interface {
	BindNamed(query string, arg interface{}) (string, []interface{}, error)
	DriverName() string
	Get(dest interface{}, query string, args ...interface{}) error
	GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	MustExec(query string, args ...interface{}) sql.Result
	Exec(query string, args ...interface{}) (sql.Result, error)
	MustExecContext(ctx context.Context, query string, args ...interface{}) sql.Result
	NamedExec(query string, arg interface{}) (sql.Result, error)
	NamedExecContext(ctx context.Context, query string, arg interface{}) (sql.Result, error)
	NamedQuery(query string, arg interface{}) (*sqlx.Rows, error)
	PrepareNamed(query string) (*sqlx.NamedStmt, error)
	PrepareNamedContext(ctx context.Context, query string) (*sqlx.NamedStmt, error)
	Preparex(query string) (*sqlx.Stmt, error)
	PreparexContext(ctx context.Context, query string) (*sqlx.Stmt, error)
	QueryRowx(query string, args ...interface{}) *sqlx.Row
	QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row
	Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
	QueryxContext(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error)
	Rebind(query string) string
	Select(dest interface{}, query string, args ...interface{}) error
	SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
}
