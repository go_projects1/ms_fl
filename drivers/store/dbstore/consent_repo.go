package dbstore

import (
	"context"
	"database/sql"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"errors"
	"time"
)

type consentRepo struct {
	logger.Logger
	store *DBStore
}

// GetByProspect
// Errors: entities.ErrSourceInternal
func (repo *consentRepo) GetByProspect(ctx context.Context, prospectID, code string) ([]entities.Consent, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT * from consents WHERE prospect_id = $1 AND offer_code=$2 ORDER BY created_at DESC`
	var records []dbconsent
	if err := sqlClient.Select(&records, q, prospectID, code); err != nil {
		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	consents := make([]entities.Consent, len(records))
	for i := 0; i < len(records); i++ {
		consents[i] = records[i].toModel()
	}
	return consents, nil
}

// GetByProspectLatest
// Errors: entities.ErrSourceInternal
func (repo *consentRepo) GetByProspectAndVersion(ctx context.Context, prospectID, code, versionCode string) (entities.Consent, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	q := `SELECT * from consents WHERE prospect_id = $1 AND offer_code=$2 AND version_code=$3 ORDER BY created_at DESC`
	r := dbconsent{}
	if err := sqlClient.Get(&r, q, prospectID, code, versionCode); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return entities.Consent{}, errs.Errf(errs.ErrNotFound, err.Error())
		}
		return entities.Consent{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	consent := r.toModel()
	return consent, nil
}

// Store
// Errors: entities.ErrSourceInternal
func (repo *consentRepo) Store(ctx context.Context, consent entities.Consent) (entities.Consent, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	consent.CreatedAt = time.Now()
	record := dbconsentFromModel(consent)
	q := `INSERT INTO consents 
			(prospect_id,sign_id,offer_code,offer_version_code,accepted_at,created_at) 
          VALUES 
			($1,$2,$3,$4,$5,$6) RETURNING id`
	err := sqlClient.Get(&consent.ID, q, record.ProspectID,
		record.SignID, record.OfferCode, record.OfferVersionCode, record.AcceptedAt, record.CreatedAt)
	if err != nil {
		return consent, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return consent, nil
}

// Update
// Errors: entities.ErrSourceInternal

func (repo *consentRepo) Update(ctx context.Context, consent entities.Consent) (entities.Consent, error) {
	sqlClient := repo.store.sqlClientByCtx(ctx)
	r := dbconsentFromModel(consent)
	q := `UPDATE consents SET prospect_id=$1,sign_id=$2,offer_code=$3,offer_version_code=$4, accepted_at=$5 WHERE id=$6`
	result, err := sqlClient.Exec(q, r.ProspectID, r.SignID, r.OfferCode, r.OfferVersionCode, r.AcceptedAt, r.ID)
	if err != nil {
		return consent, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if c, _ := result.RowsAffected(); c != 1 {
		return consent, errs.Errf(errs.ErrNotFound, "No rows affected")
	}
	return consent, nil
}
