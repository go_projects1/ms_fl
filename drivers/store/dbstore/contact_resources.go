package dbstore

import (
	"dbo/onboarding/entities"
	"time"
)

type dbcontact struct {
	ID         string     `db:"id"`
	ProspectID string     `db:"prospect_id"`
	Value      string     `db:"value"`
	Type       int8       `db:"type"`
	VerifiedAt *time.Time `db:"verified_at"`
	CreatedAt  time.Time  `db:"created_at"`
}

func (raw dbcontact) toModel() entities.Contact {
	return entities.Contact{
		ID:         raw.ID,
		ProspectID: raw.ProspectID,
		Value:      raw.Value,
		Type:       raw.Type,
		CreatedAt:  raw.CreatedAt,
		VerifiedAt: raw.VerifiedAt,
	}
}
