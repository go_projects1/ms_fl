package store

import (
	"errors"
	"fmt"
)

var (
	ErrNotFound    = errors.New("record not found")
	ErrWrongInput  = errors.New("wrong input")
	ErrInternalErr = errors.New("internal storage err")
)

type Err struct {
	Message string
	err     error
}

// E - Construct basic err
func E(err error, msg string) *Err {
	return &Err{
		Message: msg,
		err:     err,
	}
}

// Errf - Construct err + sprintf
func Errf(err error, tmpl string, args ...interface{}) *Err {
	return &Err{
		Message: fmt.Sprintf(tmpl, args...),
		err:     err,
	}
}

func (e *Err) Error() string {
	return fmt.Sprintf("%v: %v", e.err, e.Message)
}

func (e *Err) Unwrap() error {
	return e.err
}
