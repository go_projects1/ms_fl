package store

import (
	"context"
	"dbo/onboarding/contracts"
	"dbo/onboarding/entities"
)

type Store interface {
	Prospects() ProspectRepo
	Contacts() ContactRepo
	Profiles() ProfileRepo
	Consents() ConsentRepo
	Appeals() AppealRepo
	Addresses() AddressRepo

	contracts.SessionProvider
}

type ProspectRepo interface {
	contracts.IProspectSaver
	contracts.IProspectFinder
	contracts.IProspectByContactFinder
	contracts.IProspectSource
}

type ContactRepo interface {
	contracts.IContactGetter
	contracts.IContactSaver

	FindByID(ctx context.Context, id string) (entities.Contact, error)  // Find by contact value
	Find(ctx context.Context, value string) ([]entities.Contact, error) // Find by contact value
	Delete(ctx context.Context, c entities.Contact) error
}

type ProfileRepo interface {
	contracts.IProfileRetriever
	contracts.IProfileSaver
	Delete(ctx context.Context, profile entities.Profile) error
}

type ConsentRepo interface {
	contracts.IConsentSaver
	GetByProspect(ctx context.Context, prospectID, code string) ([]entities.Consent, error)
	contracts.IProspectConsentWithVersion
}

type AppealRepo interface {
	Store(context.Context, entities.Appeal) (entities.Appeal, error)
	GetByProspect(ctx context.Context, prospectID string) ([]entities.Appeal, error)
}

type AddressRepo interface {
	Store(context.Context, entities.Address) (entities.Address, error)
	Update(context.Context, entities.Address) (entities.Address, error)
	GetByProfile(ctx context.Context, profileID string) ([]entities.Address, error)
	Delete(context.Context, entities.Address) error
}
