package errs_test

import (
	"dbo/onboarding/errs"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestError_Error(t *testing.T) {
	tests := []struct {
		name string
		in   error
		want error
		same bool
	}{
		{
			"Error is the same",
			errs.Errf(errs.ErrNotFound, "Prospect id: %v", 1),
			errs.ErrNotFound,
			true,
		},
		{
			"Error not the same",
			errs.Errf(errs.ErrValidation, "Prospect id: %v", 1),
			errs.ErrNotFound,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Error(t, tt.in)
			if tt.same {
				assert.ErrorIs(t, tt.in, tt.want)
			} else {
				assert.True(t, !errors.Is(tt.in, tt.want))
			}
		})
	}
}
