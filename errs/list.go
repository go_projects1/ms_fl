package errs

var (
	ErrEntityRelationNotLoaded = New("relation is not loaded")
	ErrWrongInput              = New("wrong input")
	ErrNotFound                = New("entity not found")
	ErrSourceConnectionErr     = New("connection error")
	ErrSourceInternal          = New("internal connection error")
	// ErrValidation - External
	ErrValidation = New("validation error")

	ErrUCNoInputIsProvided = New("no input")
)
