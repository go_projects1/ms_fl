package mq

func (p *Pool) endpoints() {
	p.route("onboarding-ph",
		"consent.save",
		"onboardPh-consent-save",
		&consentSaveHandler{})
}
