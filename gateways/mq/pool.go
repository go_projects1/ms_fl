package mq

import (
	"context"
	"dbo/onboarding/controller"
	"dbo/onboarding/pkg/logger"
	"log"
	"reflect"
	"time"

	"gitlab.hamkorbank.uz/libs/rabbit"
)

const maxHandleTime = 10 * time.Second

type Worker interface {
	run(ctx context.Context) error
	init(ctrl *controller.Controller, amqp *rabbit.Consumer, log logger.Logger)
}

type Pool struct {
	logger.Logger
	ctrl    *controller.Controller
	conn    *rabbit.Connection
	workers []Worker
}

func NewPool(ctrl *controller.Controller, l logger.Logger, conn *rabbit.Connection) *Pool {
	return &Pool{
		Logger:  l,
		ctrl:    ctrl,
		conn:    conn,
		workers: make([]Worker, 0),
	}
}

func getType(handler interface{}) string {
	if t := reflect.TypeOf(handler); t.Kind() == reflect.Ptr {
		return "*" + t.Elem().Name()
	} else {
		return t.Name()
	}
}

func (p *Pool) route(exchange, routingKey, queueName string, w Worker) {
	sub, err := rabbit.NewConsumer(p.conn, exchange, routingKey, queueName, p.Logger)
	if err != nil {
		panic(err)
	}
	w.init(p.ctrl, sub, p.Logger)
	log.Printf("[MQ] %v/%v/%v => [Worker %d]: %v", exchange, routingKey, queueName, len(p.workers), getType(w))
	p.workers = append(p.workers, w)
}

func (p *Pool) Run(ctx context.Context) {
	p.endpoints()
	for i := 0; i < len(p.workers); i++ {
		go func(c context.Context, id int) {
			log.Printf("[MQ] Starting worker: %d", id)
			if err := p.workers[id].run(c); err != nil {
				panic(err)
			}
		}(ctx, i)
	}
	<-ctx.Done()
}
