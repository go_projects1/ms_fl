package mq

import (
	"context"
	"dbo/onboarding/controller"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/helpers"
	"dbo/onboarding/pkg/logger"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.hamkorbank.uz/libs/rabbit"
)

type consentSaveHandler struct {
	ctrl     *controller.Controller
	consumer *rabbit.Consumer
	log      logger.Logger
	ctx      context.Context
}

func (w *consentSaveHandler) init(ctrl *controller.Controller, consumer *rabbit.Consumer, log logger.Logger) {
	w.ctrl = ctrl
	w.consumer = consumer
	w.log = log
}

func (w *consentSaveHandler) run(ctx context.Context) error {
	w.ctx = ctx
	if err := w.consumer.Run(ctx, "[Onboarding:consentSaveHandler]", w.handle); err != nil {
		w.log.Error(err.Error())
		return err
	}
	return nil
}

func (w *consentSaveHandler) handle(e amqp.Delivery) {
	w.log.Debug(fmt.Sprintf("[consentSaveHandler]: id(%v) - received", e.MessageId))
	var requeue, ok bool

	defer func() {
		if !ok {
			_ = e.Nack(false, requeue)
			w.log.Debug(fmt.Sprintf("[consentSaveHandler]: id(%v) - Failed", e.MessageId))
		} else {
			w.log.Debug(fmt.Sprintf("[consentSaveHandler]: id(%v) - OK", e.MessageId))
		}
	}()
	ctx, cancel := context.WithTimeout(w.ctx, maxHandleTime)
	defer cancel()
	consent, err := consentSaveParseDelivery(e.Body) // Transform to Model
	if err != nil {
		w.log.Error(fmt.Sprintf("Failed to Parse: %v; with Err: %v", e.Body, err))
		return
	}
	_, err = w.ctrl.SaveConsent(ctx, consent) // Business Logic here
	if err != nil {
		w.log.Error(err.Error())
		if errors.Is(err, errs.ErrSourceInternal) {
			requeue = true
		}
		return
	}
	if err := e.Ack(false); err != nil {
		w.log.Error("AckError:" + err.Error())
	}
	ok = true
}

type m struct {
	SignID           string `json:"sign_id,omitempty"`
	OfferCode        string `json:"offer_code"`
	ProspectID       string `json:"prospect_id"`
	OfferVersionCode string `json:"offer_version_code"`
	AcceptedAt       string `json:"accepted_at"`
}

func consentSaveParseDelivery(body []byte) (entities.Consent, error) {
	var consent entities.Consent
	msg := m{}
	if err := json.Unmarshal(body, &msg); err != nil {
		return consent, err
	}
	acceptDate, err := helpers.ParseTime(msg.AcceptedAt)
	if err != nil {
		return consent, err
	}
	consent = entities.Consent{
		ProspectID:       msg.ProspectID,
		SignID:           msg.SignID,
		OfferCode:        msg.OfferCode,
		OfferVersionCode: msg.OfferVersionCode,
		AcceptedAt:       acceptDate,
	}
	return consent, nil
}
