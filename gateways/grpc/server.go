package grpc

import (
	"context"
	"dbo/onboarding/controller"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/logger"
	"dbo/onboarding/pkg/status"
	"errors"
	"fmt"
	"net"

	pb "gitlab.hamkorbank.uz/libs/protos/v2/onboarding_physical"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type GrpcServer struct {
	log  logger.Logger
	ctrl *controller.Controller
	port string
}

func New(log logger.Logger, ctrl *controller.Controller, port string) *GrpcServer {
	return &GrpcServer{
		log:  log,
		ctrl: ctrl,
		port: port,
	}
}

func (srv *GrpcServer) GetProspect(ctx context.Context, request *pb.GetProspectRequest) (*pb.GetProspectResponse, error) {
	var prospect entities.Prospect
	var err error
	if request.GetIABSClientID() == "" {
		prospect, err = srv.ctrl.GetByPhone(ctx, request.Phone, int8(request.PhoneType))
	} else {
		prospect, err = srv.ctrl.GetByIABS(ctx, request.IABSClientID, request.Phone)
	}
	switch {
	case err == nil:
		return &pb.GetProspectResponse{
			Status:    status.Success,
			ErrorCode: pb.ErrCodes_Ok,
			ErrorNote: "",
			Prospect: &pb.Prospect{
				ID:                prospect.ID,
				IABSClientID:      prospect.IABSClientID,
				RSClientID:        prospect.RSClientID,
				IsProfileComplete: prospect.IsProfileComplete,
				IsConsentNeeded:   prospect.IsConsentNeeded,
				State:             int32(prospect.State),
				IsTerrorist:       prospect.IsTerrorist,
				IsAffiliated:      prospect.IsAffiliated,
			},
		}, nil
	case errors.Is(err, errs.ErrNotFound):
		return &pb.GetProspectResponse{
			Status:    status.Failure,
			ErrorCode: pb.ErrCodes_ErrNotFound,
			ErrorNote: err.Error(),
		}, nil
	default:
		return &pb.GetProspectResponse{
			Status:    status.Failure,
			ErrorCode: pb.ErrCodes_ErrFatalCode,
			ErrorNote: err.Error(),
		}, nil
	}
}

func (srv *GrpcServer) Run(ctx context.Context) error {
	errChan := make(chan error)
	listener, err := net.Listen("tcp", srv.port)
	if err != nil {
		return err
	}
	grpcServer := grpc.NewServer()
	pb.RegisterOnboardingServer(grpcServer, srv)
	reflection.Register(grpcServer)

	defer func() {
		srv.log.Info("gRPC Server is shutting down")
		grpcServer.GracefulStop()
		srv.log.Info("gRPC Server shut down")
	}()
	go func() {
		srv.log.Info(fmt.Sprintf("Starting gRPC Server on port: %v", srv.port))
		if err := grpcServer.Serve(listener); err != nil {
			errChan <- err
		}
	}()
	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		return nil
	}
}
