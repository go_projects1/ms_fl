package rest

import (
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/status"
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// init godoc swagger
// @Summary initial request on onboarding start
// @Description Provides information about prospect. Based on statuses decision should be made
// @Router /public/initialize [GET]
// @Tags BFF
// @Accept json
// @Produce json
// @Param token query string true "ctoken"
// @Success 200 {object} R{data=respInit}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) init() gin.HandlerFunc {
	return func(c *gin.Context) {
		securedData := securedInfo(c)
		if securedData == nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "",
			})
			return
		}
		data, err := s.ctrl.FindProspect(c, securedData.ProspectID, fmt.Sprintf("%v", securedData.ProductServiceID))
		switch {
		case errors.Is(err, errs.ErrNotFound):
			c.JSON(http.StatusUnauthorized, errView(status.ErrorCodeValidation, "prospect state is invalid"))
			return
		case err == nil:
			c.JSON(http.StatusOK, view(respInit{
				Prospect: newProspect(data.Prospect),
				Offer:    newOffer(data.Offer),
			}))
		default:
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
	}
}

type respInit struct {
	Prospect prospect `json:"prospect"`
	Offer    offer    `json:"offer"`
}
