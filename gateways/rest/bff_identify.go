package rest

import (
	"dbo/onboarding/entities"
	"dbo/onboarding/pkg/status"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type reqIdentify struct {
	basicTokenData
	DocSeries       string `json:"doc_series" binding:"required"`
	DocNumber       string `json:"doc_number" binding:"required"`
	BirthDate       string `json:"birth_date"  binding:"required"`
	ConsentApproved bool   `json:"consent_approved" binding:"required"`
}

type respIdentify struct {
	Profile profile    `json:"profile"`
	Specs   fieldSpecs `json:"specifications"`
}

// identify - Process client info - doc_series, doc_number, birth_date and tries to find it in different systems
// @Summary initial request on onboarding start
// @Description Provides information about prospect. Based on statuses decision should be made
// @Router /public/identify [POST]
// @Tags BFF
// @Accept json
// @Produce json
// @Param reqIdentify body reqIdentify true "DocSeries|DocNumber|BirthDate + ctoken"
// @Success 200 {object} R{data=respInit}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) identify() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqIdentify{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		dt, err := time.Parse("2006-01-02", req.BirthDate)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity,
				errView(status.ErrorCodeValidationDateFormat, "Wrong date format"))
			return
		}
		securedData := securedInfo(c)
		if securedData == nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "",
			})
			return
		}

		// Handle request
		prospect, spec, err := s.ctrl.IdentifyFindByIdentityData(c,
			securedData.ProspectID,
			entities.IdentityData{
				DocSeries: req.DocSeries,
				DocNumber: req.DocNumber,
				BirthDate: &dt,
			})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			respIdentify{
				Profile: newProfile(prospect.FullProfile),
				Specs:   fieldSpecs(spec),
			}))
	}
}
