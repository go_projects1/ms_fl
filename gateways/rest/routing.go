package rest

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (s *Server) endpoints() {
	bff := s.router.Group("/public", s.authorize())

	bff.GET("/initialize", s.init())
	bff.GET("/identify", s.identify())
	bff.POST("/approve", s.fillMissing())
	bff.POST("/sign-init", s.initSign())
	bff.POST("/sign-verify", s.verifySign())

	s.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
