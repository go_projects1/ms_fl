package rest

import (
	"dbo/onboarding/entities"
	"dbo/onboarding/pkg/status"
	"time"
)

type R struct {
	Status    string      `json:"status"`
	ErrorCode int         `json:"error_code"`
	ErrorNote string      `json:"error_note"`
	Data      interface{} `json:"data"`
}

func view(data interface{}) R {
	return R{
		Status: status.Success,
		Data:   data,
	}
}

//
// type localized struct {
// 	Ru string `json:"ru,omitempty"`
// 	Uz string `json:"uz,omitempty"`
// 	En string `json:"en,omitempty"`
// }

func errView(code int, note string) R {
	return R{
		Status:    status.Failure,
		ErrorCode: code,
		ErrorNote: note,
		Data:      nil,
	}
}

type prospect struct {
	State       int8       `json:"state,omitempty"`
	DocSeries   string     `json:"doc_series,omitempty"`
	DocNumber   string     `json:"doc_number,omitempty"`
	PINFL       string     `json:"pinfl,omitempty"`
	BirthOfDate *time.Time `json:"birth_of_date,omitempty"`
}

func newProspect(p entities.Prospect) prospect {
	return prospect{
		State:       p.State,
		DocSeries:   p.DocSeries,
		DocNumber:   p.DocNumber,
		BirthOfDate: p.BirthDate,
		PINFL:       p.PINFL,
	}
}

type fieldSpecs entities.ProfileSpec

type profile struct {
	FirstName           string     `json:"first_name,omitempty"`
	LastName            string     `json:"last_name,omitempty"`
	MiddleName          string     `json:"middle_name,omitempty"`
	FirstNameEn         string     `json:"first_name_en,omitempty"`
	LastNameEn          string     `json:"last_name_en,omitempty"`
	BirthPlace          string     `json:"birth_place,omitempty"`
	BirthDate           *time.Time `json:"birth_date,omitempty"`
	BirthCountryCode    string     `json:"birth_country_code,omitempty"`
	Gender              string     `json:"gender,omitempty"`
	TIN                 string     `json:"tin,omitempty"`
	PINFL               string     `json:"pinfl,omitempty"`
	INPS                string     `json:"inps,omitempty"`
	NationalityCode     string     `json:"nationality_code,omitempty"`
	CitizenshipCode     string     `json:"citizenship_code,omitempty"`
	TINRegistrationDate *time.Time `json:"tin_registration_date,omitempty"`
	TINRegistrationGNI  string     `json:"tin_registration_gni,omitempty"`
}

func newProfile(p *entities.Profile) profile {
	if p == nil {
		return profile{}
	}
	return profile{
		FirstName:           p.FirstName,
		LastName:            p.LastName,
		MiddleName:          p.MiddleName,
		FirstNameEn:         p.FirstNameEn,
		LastNameEn:          p.LastNameEn,
		BirthPlace:          p.BirthPlace,
		BirthDate:           p.BirthDate,
		BirthCountryCode:    p.BirthCountryCode,
		Gender:              p.Gender,
		TIN:                 p.TIN,
		PINFL:               p.PINFL,
		INPS:                p.INPS,
		NationalityCode:     p.NationalityCode,
		CitizenshipCode:     p.CitizenshipCode,
		TINRegistrationDate: p.TINRegistrationDate,
		TINRegistrationGNI:  p.TINRegistrationGNI,
	}
}

type offer struct {
	Code         string `json:"code"`
	VersionCode  string `json:"version_code"`
	Name         string `json:"name"`
	Content      string `json:"content"`
	SignRequired bool   `json:"sign_required"`
}

func newOffer(off entities.Offer) offer {
	return offer{
		Code:         off.Code,
		VersionCode:  off.LatestVersionCode,
		Name:         off.Name,
		Content:      off.Content,
		SignRequired: off.ShouldBeSigned,
	}
}
