package rest

import (
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/pkg/status"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// fillMissing godoc swagger
// @Summary Receives user input and prospect confirmation
// @Description saves user input as profile by user input. Will use it as least prioritized. Returns redirect URL and ctoken
// @Router /public/approve [POST]
// @Tags BFF
// @Accept json
// @Produce json
// @Param reqFillMissing body reqFillMissing true "Missing fields in profile"
// @Success 200 {object} R{data=resFillMissing}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) fillMissing() gin.HandlerFunc {
	return func(c *gin.Context) {
		securedData := securedInfo(c)
		if securedData == nil || securedData.ProspectID == "" {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "",
			})
			return
		}
		req := reqFillMissing{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Wrong request data",
			})
			return
		}

		prospect, err := s.ctrl.FillMissing(c, securedData.ProspectID, req.toEntity(securedData.ProspectID))
		switch {
		case errors.Is(err, errs.ErrValidation):
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		case err == nil:
			url := s.productCatalog.GetProductURL(c, securedData.ProductServiceID)
			ctoken, err := s.encode(encodedData{
				ProspectID:       prospect.ID,
				ProductServiceID: securedData.ProductServiceID,
			})
			if err != nil {
				c.JSON(http.StatusInternalServerError, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeRemoteOther,
					ErrorNote: "Auth Redirect Failed",
				})
				return
			}
			c.JSON(http.StatusOK, R{
				Status:    status.Success,
				ErrorCode: status.NoError,
				ErrorNote: "",
				Data:      resFillMissing{RedirectURL: url, Token: ctoken},
			})
			return
		default:
			c.JSON(http.StatusInternalServerError, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeRemoteOther,
				ErrorNote: err.Error(),
				Data:      nil,
			})
			return
		}
	}
}

type reqFillMissing struct {
	FirstName           string `json:"first_name"`
	LastName            string `json:"last_name"`
	MiddleName          string `json:"middle_name"`
	FirstNameEn         string `json:"first_name_en"`
	LastNameEn          string `json:"last_name_en"`
	BirthCountryCode    string `json:"birth_country_code"`
	Gender              string `json:"gender"`
	NationalityCode     string `json:"nationality_code"`
	CitizenshipCode     string `json:"citizenship_code"`
	TINRegistrationGNI  string `json:"gni_code"`
	DocRegionCode       string `json:"doc_region_code"`
	DocDistrictCode     string `json:"doc_district_code"`
	DocSeries           string `json:"doc_series"`
	DocNumber           string `json:"doc_number"`
	AddressRegionCode   string `json:"domicile_region"`
	AddressDistrictCode string `json:"domicile_district"`
	AddressCountryCode  string `json:"domicile_country"`
}

func (r *reqFillMissing) toEntity(prospectID string) entities.Profile {
	p := entities.Profile{
		Source:              entities.ProfileSourceUserInput,
		ProspectID:          prospectID,
		FirstName:           r.FirstName,
		LastName:            r.LastName,
		MiddleName:          r.MiddleName,
		FirstNameEn:         r.FirstNameEn,
		LastNameEn:          r.LastNameEn,
		BirthPlace:          "",
		BirthDate:           nil,
		BirthCountryCode:    r.BirthCountryCode,
		Gender:              r.Gender,
		TIN:                 "",
		PINFL:               "",
		INPS:                "",
		NationalityCode:     r.NationalityCode,
		CitizenshipCode:     r.CitizenshipCode,
		TINRegistrationDate: nil,
		TINRegistrationGNI:  r.TINRegistrationGNI,
		SubjectState:        "",
		Document: entities.Document{
			DocExpiryDate:   nil,
			DocIssueDate:    nil,
			DocIssueOrgCode: r.DocRegionCode + r.DocDistrictCode,
			DocIssueOrgDesc: "",
			DocRegionCode:   r.DocRegionCode,
			DocDistrictCode: r.DocDistrictCode,
		},
	}
	addr := entities.Address{
		Type:         entities.AddressTypeDomicile,
		Address:      "",
		RegisteredAt: nil,
		CountryCode:  r.AddressCountryCode,
		RegionCode:   r.AddressRegionCode,
		DistrictCode: r.AddressDistrictCode,
		Block:        "",
		Flat:         "",
		House:        "",
		PlaceDesc:    "",
		StreetDesc:   "",
		Kadastr:      "",
	}
	p.SetAddresses([]entities.Address{addr})
	return p
}

type resFillMissing struct {
	RedirectURL string `json:"redirect_url,omitempty"`
	Token       string `json:"ctoken"`
}
