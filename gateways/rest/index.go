package rest

import (
	"dbo/onboarding/contracts"
	"dbo/onboarding/controller"
	"dbo/onboarding/drivers/services"
	"dbo/onboarding/pkg/cors"
	"dbo/onboarding/pkg/logger"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Server struct {
	logger.Logger
	ctrl           controller.Controller
	router         *gin.Engine
	auth           services.Security
	productCatalog contracts.IProductCatalog
}

func New(port string, log logger.Logger,
	ctrl controller.Controller,
	auth services.Security,
	productCatalog contracts.IProductCatalog) *http.Server {
	r := gin.New()
	r.Use(cors.CORSMiddleware())
	srv := &Server{
		Logger:         log,
		ctrl:           ctrl,
		router:         r,
		auth:           auth,
		productCatalog: productCatalog,
	}
	srv.endpoints()
	httpServer := &http.Server{
		Addr:    port,
		Handler: srv,
	}
	srv.Info(fmt.Sprintf("HTTP server is initialized on port: %v", port))
	return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
