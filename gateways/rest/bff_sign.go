package rest

import (
	"dbo/onboarding/pkg/status"
	"net/http"

	"github.com/gin-gonic/gin"
)

type initSignRes struct {
	SignID string `json:"sign_id"`
}

// initSign godoc swagger
// @Summary Request to sign offer with OTP
// @Description Sends Authorization code to verified Contact of type MobilePhone | MobileBanking
// @Router /public/sign-init [POST]
// @Tags BFF
// @Accept json
// @Produce json
// @Param ctoken query string true "Token containing ProspectID"
// @Success 200 {object} R{data=initSignRes}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) initSign() gin.HandlerFunc {
	return func(c *gin.Context) {
		securedData := securedInfo(c)
		if securedData == nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "",
			})
			return
		}
		prospectID := securedData.ProspectID
		signID, err := s.ctrl.GenerateSignCode(c, prospectID)
		if err != nil {
			c.JSON(http.StatusInternalServerError, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeRemoteOther,
				ErrorNote: err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, R{
			Status:    status.Success,
			ErrorCode: status.NoError,
			Data:      initSignRes{SignID: signID},
		})
	}
}

type initSignReq struct {
	basicTokenData
	SignID           string `json:"sign_id" binding:"required"`
	Code             string `json:"code" binding:"required"`
	OfferCode        string `json:"offer_code" binding:"required"`
	OfferVersionCode string `json:"offer_version_code" binding:"required"`
}

// verifySign godoc swagger
// @Summary Checks code input
// @Description Checks OTP and saves OTP fact in case of success based on prospectID generated from token
// @Router /public/sign-verify [POST]
// @Tags BFF
// @Accept json
// @Produce json
// @Param request body initSignReq true "Sign ID version code etc"
// @Success 200 {object} R
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) verifySign() gin.HandlerFunc {
	return func(c *gin.Context) {
		securedData := securedInfo(c)
		if securedData == nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "",
			})
			return
		}
		req := initSignReq{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		prospectID := securedData.ProspectID
		err := s.ctrl.VerifySignCode(c, prospectID, req.OfferCode, req.OfferVersionCode, req.Code, req.SignID)
		if err != nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeRemoteOther,
				ErrorNote: err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, R{
			Status:    status.Success,
			ErrorCode: status.NoError,
		})
	}
}
