package rest

import (
	"dbo/onboarding/pkg/status"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/mitchellh/mapstructure"
)

const tokenDataKey = "ctokenKey"
const tokenParamName = "ctoken"

type basicTokenData struct {
	Token string `json:"ctoken" binding:"required"`
}

type encodedData struct {
	Phone            string `mapstructure:"phone,omitempty"`
	ProspectID       string `mapstructure:"prospect_id,omitempty"`
	IABSClientID     string `mapstructure:"iabs_client_id,omitempty"`
	ProductServiceID uint32 `mapstructure:"product_service_id,omitempty"`
}

func (s *Server) authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		req := basicTokenData{}
		if token := c.Query(tokenParamName); token != "" {
			req.Token = token
		}
		if req.Token == "" {
			if err := c.ShouldBindJSON(&req); err != nil {
				c.JSON(http.StatusUnauthorized, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: "not authorized",
				})
				c.Abort()
				return
			}
		}
		decodedData, err := s.decode(req.Token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "not authorized",
			})
			c.Abort()
			return
		}
		c.Set(tokenDataKey, decodedData)
		c.Next()
	}
}

func (s *Server) decode(token string) (*encodedData, error) {
	data, err := s.auth.DecodeToken(token)
	if err != nil {
		return nil, err
	}
	res := encodedData{}
	if err := mapstructure.Decode(data, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

func (s *Server) encode(data encodedData) (string, error) {
	toEncode := make(map[string]interface{})
	if err := mapstructure.Decode(data, &toEncode); err != nil {
		return "", err
	}
	token, err := s.auth.EncodeData(toEncode)
	if err != nil {
		return "", err
	}
	return token, nil
}

func securedInfo(c *gin.Context) *encodedData {
	data, ok := c.Get(tokenDataKey)
	if !ok {
		return nil
	}
	res, ok := data.(*encodedData)
	if !ok {
		return nil
	}
	return res
}
