package contracts

import (
	"context"
	"dbo/onboarding/entities"

	"gitlab.hamkorbank.uz/libs/protos/v2/reference"
)

// Interfaces that exists in DB

/**
* Prospect related interfaces
 */

// IProspectSource prospects repo
type IProspectSource interface {
	Get(ctx context.Context, id string) (entities.Prospect, error)
}

// IProspectFinder retrieves Prospect by identity data - Local
type IProspectFinder interface {
	Find(ctx context.Context, data entities.IdentityData) (entities.Prospect, error)
}

// IProspectSaver - Store and Update entities.Prospect
type IProspectSaver interface {
	Store(ctx context.Context, prospect entities.Prospect) (entities.Prospect, error)
	Update(ctx context.Context, prospect entities.Prospect) (entities.Prospect, error)
}

// IProspectByContactFinder - Gets prospect by contact
type IProspectByContactFinder interface {
	GetByContact(ctx context.Context, c entities.Contact) (entities.Prospect, error)
}

/**
* Profile related interfaces
 */

// IProfileSearcher - searches profile by identity data
type IProfileSearcher interface {
	Search(ctx context.Context, data entities.IdentityData) (entities.Profile, error)
}

// IProfileSaver - Store and Update entities.Profile
type IProfileSaver interface {
	Store(ctx context.Context, profile entities.Profile) (entities.Profile, error)
	Update(ctx context.Context, profile entities.Profile) (entities.Profile, error)
}

type IProfileRetriever interface {
	GetByProspect(ctx context.Context, prospectID string) ([]entities.Profile, error)
}

/**
* Address related interfaces
 */

// IAddressSaver create or update
type IAddressSaver interface {
	Store(ctx context.Context, c entities.Address) (entities.Address, error)
	Update(ctx context.Context, c entities.Address) (entities.Address, error)
}

// IAddressLoader - Returns addresses by profile ID
type IAddressLoader interface {
	GetByProfile(ctx context.Context, profileID string) ([]entities.Address, error)
}

/**
* Consent related interfaces
 */

// IConsentSaver create or update
type IConsentSaver interface {
	Store(ctx context.Context, c entities.Consent) (entities.Consent, error)
	Update(ctx context.Context, c entities.Consent) (entities.Consent, error)
}

type IProspectConsentWithVersion interface {
	GetByProspectAndVersion(ctx context.Context, prospectID, code, versionCode string) (entities.Consent, error)
}

type IMasterOfferSource interface {
	GetOffer(ctx context.Context, offerCode string, lang string) (entities.Offer, error)
}

/**
* Contact related interfaces
 */

type IContactSaver interface {
	Store(ctx context.Context, contact entities.Contact) (entities.Contact, error)
	Update(ctx context.Context, contact entities.Contact) (entities.Contact, error)
}

type IAppealStore interface {
	Store(ctx context.Context, contact entities.Appeal) (entities.Appeal, error)
}

type IContactGetter interface {
	GetByProspect(ctx context.Context, prospectID string) ([]entities.Contact, error)
}

type IReferences interface {
	Get(ctx context.Context, codes ...reference.RefID) (entities.Dictionaries, error)
}

type IProductCatalog interface {
	GetProductURL(ctx context.Context, productServiceID uint32) string
}

type IChecker interface {
	Check(ctx context.Context, prospect entities.Prospect) (entities.CheckResults, error)
}

type ISignatureService interface {
	InitSign(ctx context.Context, contact entities.Contact) (signatureID string, err error)
	Sign(ctx context.Context, code, signatureID string) error
}
