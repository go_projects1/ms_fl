package contracts

import (
	"context"
)

type SessionProvider interface {
	StartSession(c context.Context) (s Session, ctx context.Context, err error)
}

type Session interface {
	Close(err error)
}
