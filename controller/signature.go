package controller

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/errs"
	"dbo/onboarding/usecases"
)

func (ctrl *Controller) GenerateSignCode(ctx context.Context, prospectID string) (string, error) {
	uc := usecases.GenerateOTP(ctrl.serviceContainer.Signature(), ctrl.localStore.Contacts())
	uc.Params(usecases.GenerateOTPIn{ProspectID: prospectID})
	if err := uc.Execute(ctx); err != nil {
		return "", err
	}
	return uc.Results().SignatureID, nil
}

func (ctrl *Controller) VerifySignCode(ctx context.Context, prospectID, offerCode, offerVersionCode, code, signID string) error {
	uc := usecases.VerifyOTP(ctrl.serviceContainer.Signature(), ctrl.localStore.Consents())
	uc.Params(usecases.VerifyOTPIn{
		Offer: entities.Offer{
			Code:              offerCode,
			LatestVersionCode: offerVersionCode,
		},
		Code:        code,
		SignatureID: signID,
		ProspectID:  prospectID,
	})
	if err := uc.Execute(ctx); err != nil {
		return err
	}
	if !uc.Results().IsSuccess {
		return errs.Errf(errs.ErrSourceInternal, "Failed to sign")
	}
	return nil
}
