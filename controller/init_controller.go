package controller

import (
	"dbo/onboarding/drivers/services"
	"dbo/onboarding/drivers/store"
	"dbo/onboarding/pkg/logger"
	"fmt"
)

type Controller struct {
	log              logger.Logger
	serviceContainer services.Container
	localStore       store.Store
}

func New(l logger.Logger, serviceContainer services.Container, localStore store.Store) Controller {
	ctrl := Controller{
		log:              l,
		serviceContainer: serviceContainer,
		localStore:       localStore,
	}
	ctrl.debugf("Controller is being created")
	ctrl.infof("Controller is created")
	return ctrl
}

func (ctrl Controller) errorf(tmpl string, params ...interface{}) {
	ctrl.log.Error(fmt.Sprintf("controller: "+tmpl, params...))
}

func (ctrl Controller) debugf(tmpl string, params ...interface{}) {
	ctrl.log.Debug(fmt.Sprintf("controller: "+tmpl, params...))
}

func (ctrl Controller) infof(tmpl string, params ...interface{}) {
	ctrl.log.Debug(fmt.Sprintf("controller: "+tmpl, params...))
}
