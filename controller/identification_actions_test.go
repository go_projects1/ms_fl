package controller_test

import (
	"context"
	"dbo/onboarding/entities"
	"reflect"
	"testing"
	"time"
)

func TestController_FillMissing(t *testing.T) {
	type args struct {
		ctx        context.Context
		prospectID string
		profile    entities.Profile
	}
	tests := []struct {
		name    string
		args    args
		want    entities.Prospect
		wantErr bool
	}{
		// {
		// 	"Success"
		// 	context.Background(),
		// }
	}
	ctrl := testingCtrl(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ctrl.FillMissing(tt.args.ctx, tt.args.prospectID, tt.args.profile)
			if (err != nil) != tt.wantErr {
				t.Errorf("FillMissing() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FillMissing() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func date(str string) *time.Time {
	bd, _ := time.Parse("2006-01-02", str)
	return &bd
}

func TestController_IdentifyFindByIdentityData(t *testing.T) {
	type args struct {
		ctx        context.Context
		prospectID string
		identity   entities.IdentityData
	}

	tests := []struct {
		name    string
		args    args
		want    entities.Prospect
		want1   entities.ProfileSpec
		wantErr bool
	}{
		{
			name: "Test",
			args: args{
				ctx:        context.Background(),
				prospectID: "d3ae7a2e-dc19-4eae-acd2-e28d02216409",
				identity: entities.IdentityData{
					PINFL:        "",
					DocSeries:    "AA",
					DocNumber:    "1918563",
					BirthDate:    date("1993-11-10"),
					IABSClientID: "",
				},
			},
			want:    entities.Prospect{},
			want1:   nil,
			wantErr: false,
		},
		{
			name: "Test",
			args: args{
				ctx:        context.Background(),
				prospectID: "d3ae7a2e-dc19-4eae-acd2-e28d02216409",
				identity: entities.IdentityData{
					PINFL:        "",
					DocSeries:    "AB",
					DocNumber:    "1573218",
					BirthDate:    date("1999-08-29"),
					IABSClientID: "",
				},
			},
			want:    entities.Prospect{},
			want1:   nil,
			wantErr: false,
		},
	}
	ctrl := testingCtrl(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, _, err := ctrl.IdentifyFindByIdentityData(tt.args.ctx, tt.args.prospectID, tt.args.identity)
			if (err != nil) != tt.wantErr {
				t.Errorf("IdentifyFindByIdentityData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
