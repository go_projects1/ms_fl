package controller

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/usecases"
)

func (ctrl *Controller) SaveConsent(ctx context.Context, c entities.Consent) (entities.Consent, error) {
	var processErr error
	session, ctx, err := ctrl.localStore.StartSession(ctx)
	if err != nil {
		return c, err
	}
	defer func() {
		if processErr != nil {
			ctrl.errorf("SaveConsent %v", processErr)
		}
		session.Close(processErr)
	}()

	uc := usecases.SaveConsent{
		ConsentSaver:   ctrl.localStore.Consents(),
		ProspectSource: ctrl.localStore.Prospects(),
		ProspectSaver:  ctrl.localStore.Prospects(),
	}
	uc.Params(usecases.InSaveConsent{Consent: c})
	processErr = uc.Execute(ctx)
	if processErr != nil {
		return entities.Consent{}, processErr
	}
	result := uc.Result()

	return result.Consent, nil
}

func (ctrl *Controller) GetByIABS(ctx context.Context, iabsClientID, phone string) (entities.Prospect, error) {
	var prospect entities.Prospect
	var processErr error

	session, ctx, err := ctrl.localStore.StartSession(ctx)
	if err != nil {
		return prospect, err
	}
	defer func() {
		if processErr != nil {
			ctrl.errorf("GetByIABS %v", processErr)
		}
		session.Close(processErr)
	}()

	prospects := ctrl.localStore.Prospects()
	contacts := ctrl.localStore.Contacts()
	uc := usecases.ProspectByIABS{
		Logger:                       ctrl.log,
		LocalProspectByContactFinder: prospects,
		LocalProspectFinder:          prospects,
		ProspectSaver:                prospects,
		IabsProfileFinder:            ctrl.serviceContainer.Clients(),
		ContactSaver:                 contacts,
		LocalContactSource:           contacts,
	}
	uc.Params(usecases.InProspectByIABS{
		Contact:      entities.Contact{Type: entities.ContactTypePhoneMobile, Value: phone},
		IABSClientID: iabsClientID,
	})
	processErr = uc.Execute(ctx)
	if processErr != nil {
		return prospect, processErr
	}
	prospect = uc.Result().Prospect
	return prospect, nil
}

func (ctrl *Controller) GetByPhone(ctx context.Context, phone string, phoneType int8) (entities.Prospect, error) {
	var prospect entities.Prospect
	var processErr error

	session, ctx, err := ctrl.localStore.StartSession(ctx)
	if err != nil {
		return prospect, err
	}
	defer func() {
		if processErr != nil {
			ctrl.errorf("GetByPhone %v", processErr)
		}
		session.Close(processErr)
	}()

	prospects := ctrl.localStore.Prospects()
	contacts := ctrl.localStore.Contacts()
	uc := usecases.ProspectByPhone{
		ProspectByPhoneFinder: prospects,
		ProspectSaver:         prospects,
		ContactSaver:          contacts,
	}
	uc.Params(usecases.InProspectByPhone{
		Contact: entities.Contact{Type: phoneType, Value: phone},
	})
	processErr = uc.Execute(ctx)
	if processErr != nil {
		return prospect, processErr
	}
	prospect = uc.Result().Prospect

	return prospect, nil
}

func (ctrl *Controller) FindProspect(ctx context.Context, prospectID, productID string) (*usecases.FindProspectOut, error) {
	uc := usecases.FindProspect(
		ctrl.localStore.Prospects(),
		ctrl.serviceContainer.Offers(),
		ctrl.localStore.Consents(),
		ctrl.localStore.Appeals())
	uc.Params(usecases.FindProspectIn{ProspectID: prospectID, ProductID: productID})

	if err := uc.Execute(ctx); err != nil {
		ctrl.errorf("FindProspect %v", err)
		return nil, err
	}
	return uc.Result(), nil
}
