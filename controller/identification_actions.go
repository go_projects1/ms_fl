package controller

import (
	"context"
	"dbo/onboarding/entities"
	"dbo/onboarding/usecases"
)

func (ctrl *Controller) IdentifyFindByIdentityData(
	ctx context.Context, prospectID string, identity entities.IdentityData) (
	entities.Prospect, entities.ProfileSpec, error) {
	// Declare results
	var prospect entities.Prospect
	var specs entities.ProfileSpec
	// Start session
	var processErr error
	session, ctx, err := ctrl.localStore.StartSession(ctx)
	if err != nil {
		return prospect, specs, err
	}
	defer func() { session.Close(processErr) }()

	uc := usecases.Identify{
		ProspectSourceRepo: ctrl.localStore.Prospects(),
		ProfilesIABS:       ctrl.serviceContainer.Clients(),
		ProfilesNIBBD:      ctrl.serviceContainer.NIBBD(),
		ProfileSaverRepo:   ctrl.localStore.Profiles(),
		References:         ctrl.serviceContainer.References(),
		ProspectSaverRepo:  ctrl.localStore.Prospects(),
		AddressSaverRepo:   ctrl.localStore.Addresses(),
	}
	uc.Params(usecases.InIdentify{
		ProspectID: prospectID,
		Identity:   identity,
	})
	if processErr = uc.Execute(ctx); processErr != nil {
		ctrl.log.Debug(processErr.Error())
		return prospect, specs, processErr
	}
	prospect = uc.Result().Prospect
	specs = uc.Result().Specs

	return prospect, specs, nil
}

func (ctrl *Controller) FillMissing(ctx context.Context, prospectID string, profile entities.Profile) (
	entities.Prospect, error) {
	var prospect entities.Prospect
	var processErr error

	session, ctx, err := ctrl.localStore.StartSession(ctx)
	if err != nil {
		return prospect, err
	}
	defer func() { session.Close(processErr) }()

	uc := usecases.FillMissingAndApprove{
		LocalProspectSource:   ctrl.localStore.Prospects(),
		LocalProfileRetriever: ctrl.localStore.Profiles(),
		ProfileIABS:           ctrl.serviceContainer.Clients(),
		References:            ctrl.serviceContainer.References(),
		AddressLoader:         ctrl.localStore.Addresses(),
		ProfileSaver:          ctrl.localStore.Profiles(),
		ProspectSaver:         ctrl.localStore.Prospects(),
	}
	uc.Params(usecases.InFillMissing{
		Profile:    profile,
		ProspectID: prospectID,
	})
	processErr = uc.Execute(ctx)
	if processErr != nil {
		return prospect, processErr
	}
	prospect = uc.Result().Prospect
	return prospect, nil
}
