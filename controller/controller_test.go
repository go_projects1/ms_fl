package controller_test

import (
	"dbo/onboarding/config"
	"dbo/onboarding/controller"
	"dbo/onboarding/drivers/services/service_registry"
	"dbo/onboarding/drivers/store/dbstore"
	"dbo/onboarding/pkg/logger"
	"testing"

	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
)

func testingCtrl(t *testing.T) *controller.Controller {
	t.Helper()
	l := logger.New("debug", "test_db")
	cfg := config.Load()
	db, err := sqlx.Connect("postgres", cfg.PostgresURL())
	if err != nil {
		panic(err.Error())
	}
	s := dbstore.New(l, db)
	sr := service_registry.New(cfg, l)
	ctrl := controller.New(l, &sr, s)
	return &ctrl
}
